<?php

class ComponentEntityTemplateBuilder extends EntityTemplateBuilderBase {
  use TemplateComponentsFormTrait {
    configurationForm as traitConfigurationForm;
    configurationFormValidate as traitConfigurationFormValidate;
    configurationFormSubmit as traitConfigurationFormSubmit;
  }

  /**
   * {@inheritdoc}
   */
  public function buildTemplate(): object {
    $components = $this->template->getTemplate();

    return new EntityTemplateConfiguration(
      $this->template->identifier() ?: uniqid(),
      $this->template->getTargetEntityType(),
      $this->template->getTargetBundle(),
      $components ?: [],
      $this->template->getParameters() ?: [],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function executeTemplate(array $parameters, EntityDrupalWrapper $target = NULL): object {
    /** @var \EntityTemplateConfiguration $template */
    $template = $this->buildTemplate();

    $entity_info = entity_get_info($template->getEntityType());

    if ($target !== NULL) {
      $structure = $target;
    }
    else {
      $values = [
        '__template' => $this->template,
      ];
      if (!empty($entity_info['entity keys']['bundle'])) {
        $values[$entity_info['entity keys']['bundle']] = $template->getBundle();
      }
      $entity = entity_create($template->getEntityType(), $values);
      module_invoke_all('entity_template_prepare_blank_template', $this->template, $entity, $template->getEntityType());

      $structure = entity_metadata_wrapper($template->getEntityType(), $entity);
    }

    $parameters['self'] = $structure;
    $parameters['site'] = entity_metadata_wrapper('site', FALSE, [
      'type' => 'site',
      'label' => t('Site Information'),
      'description' => t('Site-wide settings and other global information.'),
      'property info alter' => ['RulesData', 'addSiteMetadata'],
      'property info' => [],
      'optional' => TRUE,
    ]);
    foreach ($template->getComponents(TRUE) as $component) {
      $component->execute($structure, $parameters);
    }

    if ($target === NULL) {
      // Finally tell the entity which template made it.
      $entity->entity_template = $this->template->identifier();

      module_invoke_all(
        'entity_template_create_entity',
        $this->template,
        $entity,
        $this->template->getTargetEntityType()
      );
      rules_invoke_event(
        'entity_template_create_entity',
        $this->template,
        entity_metadata_wrapper($this->template->getTargetEntityType(), $entity)
      );
    }

    return $structure->value();
  }

  /**
   * {@inheritdoc}
   */
  protected function ensureRelevantTemplateConfig(array $form, array &$form_state): TemplateConfiguration {
    $builder_form_state = &static::getConfigurationFormState($form, $form_state);
    $builder_form_state['template_config'] = $builder_form_state['template_config'] ?? $this->buildTemplate();
    $builder_form_state['template_config']->setFormStateAddress(array_merge(
      ['builder_state'],
      static::getRootFormElement($form, $form_state['complete form'] ?? [])['#parents'] ?? [],
      ['#state', 'template_config']
    ));
    return $builder_form_state['template_config'];
  }

  /**
   * {@inheritdoc}
   */
  protected static function &getConfigurationFormState(array $form, array &$form_state): array {
    $builder_form = static::getRootFormElement($form, $form_state['complete form'] ?? []);
    if (!isset($form_state['builder_state'])) {
      $form_state['builder_state'] = [];
    }

    $addr = $builder_form['#parents'];
    $addr[] = '#state';
    $cs = &drupal_array_get_nested_value($form_state['builder_state'], $addr, $ke);

    if (!$ke) {
      drupal_array_set_nested_value($form_state['builder_state'], $addr, []);
      $cs = &drupal_array_get_nested_value($form_state['builder_state'], $addr);
    }

    return $cs;
  }

  protected static function getRootFormElement(array $element, array $form): array {
    if (!empty($element['#template_builder_root_element'])) {
      return $element;
    }

    $array_parents = $element['#array_parents'];
    do {
      if (
        ($element = drupal_array_get_nested_value($form, $array_parents)) &&
        !empty($element['#template_builder_root_element'])
      ) {
        return $element;
      }
    } while($array_parents = array_slice($array_parents, 0, -1));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, array &$form_state): array {
    $form['#template_builder_root_element'] = TRUE;
    return $this->traitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function configurationFormValidate(array $form, array &$form_state) {
    $this->traitConfigurationFormValidate($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function configurationFormSubmit(array $form, array &$form_state) {
    $this->traitConfigurationFormSubmit($form, $form_state);

    $template_conf = [];
    foreach ($this->ensureRelevantTemplateConfig($form, $form_state)->getComponents() as $id => $component) {
      $template_conf[$id] = [
        'plugin' => $component->getPluginId(),
        'definition' => array_diff_key($component->getDefinition(), ['label' => TRUE, 'class' => TRUE]),
        'configuration' => $component->getConfiguration(),
        'priority' => $component->getPriority(),
        'conditions' => $component->getConditions(),
      ];
    }
    $this->template->setTemplate($template_conf);
  }

}
