<?php
/**
 * @file
 *   Entity Template Module code.
 */

/**
 * Implements hook_menu().
 */
function entity_template_menu() {
  $items['template/select'] = array(
    'title callback' => 'entity_template_select_template_page_title',
    'page callback' => 'entity_template_create_entity_page',
    'page arguments' => array(),
    'access callback' => 'entity_template_access',
    'access arguments' => array('use'),
    'file' => 'entity_template.ui.inc',
  );

  $items['template/select/%ctools_js'] = array(
    'title callback' => 'entity_template_select_template_page_title',
    'page callback' => 'entity_template_create_entity_page',
    'page arguments' => array(NULL, 2),
    'access callback' => 'entity_template_access',
    'access arguments' => array('use'),
    'file' => 'entity_template.ui.inc',
  );

  $items['template/add/%entity_template'] = array(
    'title callback' => 'entity_template_create_entity_page_title',
    'title arguments' => array(2),
    'page callback' => 'entity_template_create_entity_page',
    'page arguments' => array(2),
    'access callback' => 'entity_template_access',
    'access arguments' => array('use', 2),
    'file' => 'entity_template.ui.inc',
  );

  $items['template/add/%entity_template/%ctools_js'] = array(
    'title callback' => 'entity_template_create_entity_page_title',
    'title arguments' => array(2),
    'page callback' => 'entity_template_create_entity_page',
    'page arguments' => array(2, 3),
    'access callback' => 'entity_template_access',
    'access arguments' => array('use', 2),
    'theme callback' => 'ajax_base_page_theme',
    'file' => 'entity_template.ui.inc',
  );

  $items['template/autocomplete/tags'] = array(
    'title' => 'Template Tags Autocomplete',
    'page callback' => 'entity_template_tags_autocomplete_callback',
    'page arguments' => array(),
    'access arguments' => array('administer entity_templates'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Title callback for entity template select page.
 */
function entity_template_select_page_title() {
  return t('Select Template');
}

/**
 * Title callback for entity template create entity page.
 */
function entity_template_create_entity_page_title($template) {
  return t('Add @label', array('@label' => $template->label));
}

/**
 * Implements hook_hook_info().
 */
function entity_template_hook_info() {
  $hooks = array();

  $hooks['entity_template_component_plugin_info'] = array('group' => 'entity_template.component');
  $hooks['entity_template_component_plugin_info_alter'] = array('group' => 'entity_template.component');

  $hooks['entity_template_component_modifier_plugin_info'] = array('group' => 'entity_template.component');
  $hooks['entity_template_component_modifier_plugin_info_alter'] = array('group' => 'entity_template.component');

  return $hooks;
}

/**
 * Implements hook_entity_info().
 */
function entity_template_entity_info() {
  $info['entity_template'] = array(
    'label' => t('Entity Template'),
    'description' => t('Stores a Template for an entity'),
    'entity class' => 'EntityTemplate',
    'controller class' => 'EntityTemplateController',
    'base table' => 'entity_template',
    'revision table' => 'entity_template_revision',
    'fieldable' => TRUE,
    'exportable' => TRUE,
    'uuid' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'uuid' => 'uuid',
      'revision' => 'vid',
      'name' => 'name',
      'label' => 'label',
    ),
    'access callback' => 'entity_template_access',
    'module' => 'entity_template',
    'admin ui' => array(
      'path' => 'admin/content/templates',
      'file' => 'entity_template.admin.inc',
      'controller class' => 'EntityTemplateUIController',
    ),
    'bundles' => array(
      'entity_template' => array(
        'label' => t('Entity Template'),
        'admin' => array(
          'path' => 'admin/config/content/entity_template',
          'access arguments' => array(
            0 => 'administer entity_templates',
          ),
        ),
      ),
    ),
  );

  if (module_exists('entitycache')) {
    $info['entity_template']['field cache'] = FALSE;
    $info['entity_template']['entity cache'] = TRUE;
  }

  return $info;
}

/**
 * Implements hook_forms().
 */
function entity_template_forms($form_id, $args) {
  $forms['entity_template_entity_create_form'] = array(
    'callback' => 'entity_template_entity_create_form',
    'wrapper_callback' => 'entity_template_entity_create_form_wrapper',
  );

  return $forms;
}

/**
 * Implements hook_views_api().
 */
function entity_template_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'entity_template') . '/views',
  );
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function entity_template_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'ctools' && ($plugin_type == 'content_types' || $plugin_type == 'contexts')) {
    return "plugins/$plugin_type";
  }
}

/**
 * Implements hook_permission().
 */
function entity_template_permission() {
  $perms['administer entity_templates'] = array(
    'title' => t('Administer Entity Templates'),
  );

  foreach (entity_template_access_groups() as $group => $label) {
    $perms['edit '.$group.' entity_templates'] = array(
      'title' => t('Edit %group Templates', array('%group' => $label)),
    );

    $perms['use '.$group.' entity_templates'] = array(
      'title' => t('Use %group Templates', array('%group' => $label)),
    );
  }

  return $perms;
}

/**
 * Get all the templates that create a given entity type.
 *
 * @param string $entity_type
 *   The entity type that the templates should create.
 * @param string $bundle (optional)
 *   The bundle to search for.
 */
function entity_template_get_templates($entity_type, $bundle = '') {
  $q = new EntityFieldQuery();
  $q->entityCondition('entity_type', 'entity_template');
  $q->propertyCondition('entity_type', $entity_type);
  if (!empty($bundle)) {
    $q->propertyCondition('bundle', $bundle);
  }
  $result = $q->execute();

  if (empty($result['entity_template'])) {
    return array();
  }

  return entity_load('entity_template', array_keys($result['entity_template']));
}

/**
 * Entity Template Load Callback.
 */
function entity_template_load($id, $vid = FALSE) {
  if (is_numeric($vid)) {
    return entity_revision_load('entity_template', $vid);
  }

  return entity_load_single('entity_template', $id);
}

/**
 * Entty Template Access callback.
 */
function entity_template_access($op, $template = FALSE, $account = NULL) {
  global $user;

  if (empty($account)) {
    $account = $user;
  }

  if (user_access('administer entity_templates', $account)) {
    return TRUE;
  }

  if ($template) {
    $access_group = $template->entity_template_access_group[LANGUAGE_NONE][0]['value'];

    if ($op == 'edit') {
      return user_access('edit '.$access_group.' entity_templates', $account);
    }

    if ($op == 'use') {
      return user_access('use '.$access_group.' entity_templates', $account);
    }
  }
}

/**
 * Implements hook_entity_template_presave().
 */
function entity_template_entity_template_presave($template) {
  global $user;

  if (!isset($template->is_new_revision)) {
    $template->is_new_revision = TRUE;
  }

  if ($template->is_new_revision) {
    $template->uid = $user->uid;
    $template->timestamp = REQUEST_TIME;
  }
}

/**
 * Implements hook_form_alter().
 */
function entity_template_form_views_exposed_form_alter(&$form, &$form_state) {
  $view = $form_state['view'];
  if ($view->base_table != 'entity_template') {
    return;
  }

  if (empty($form['entity_type']) || empty($form['bundle'])) {
    return;
  }

  $form['entity_type']['#ajax'] = array(
    'wrapper' => 'entity-template-views-bundle-filter-wrapper',
    'method' => 'replace',
    'callback' => 'entity_template_views_exposed_form_ajax',
  );
  $form['bundle']['#prefix'] = '<div id="entity-template-views-bundle-filter-wrapper">';
  $form['bundle']['#suffix'] = '</div>';

  $entity_type = $form['entity_type']['#default_value'];
  if (!empty($form_state['input']['entity_type'])) {
    $entity_type = $form_state['input']['entity_type'];
  }

  if (entity_get_info($entity_type)) {
    $form['bundle']['#options'] = entity_template_bundle_options_list($entity_type); // + array('All' => t('- Any -'));
  }
}

/**
 * Ajax callback for filters.
 */
function entity_template_views_exposed_form_ajax($form, $form_state) {
  return $form['bundle'];
}

/**
 * Get entity template status options list.
 */
function entity_template_status_options_list() {
  return array(
    ENTITY_CUSTOM => t('Custom'),
    ENTITY_IN_CODE => t('Default'),
    ENTITY_OVERRIDDEN => t('Overridden'),
  );
}

/**
 * Get a list of options for the entity type.
 */
function entity_template_entity_type_options_list() {
  $info = entity_get_info();
  $entity_type_options = array();
  foreach ($info as $entity_type => $eInfo) {
    $entity_type_options[$entity_type] = $eInfo['label'];
  }
  return $entity_type_options;
}

/**
 * Get a list of options for the bundle field.
 */
function entity_template_bundle_options_list($type = FALSE) {
  $info = entity_get_info();
  $options = array();
  foreach($info as $entity_type => $eInfo) {
    $options[$entity_type] = array();
    foreach ($info[$entity_type]['bundles'] as $bundle => $bInfo) {
      $options[$entity_type][$bundle] = $bInfo['label'];
    }

    if (empty($options[$entity_type])) {
      $options[$entity_type][$entity_type] = $eInfo[$entity_type]['label'];
    }
  }

  if (!empty($type) && ($type != 'bundle')) {
    return $options[$type];
  }

  foreach ($options as $type => $type_options) {
    $options[$info[$type]['label']] = $type_options;
    unset($options[$type]);
  }

  return $options;
}

/**
 * Make an Entity From a Template.
 *
 * @return stdClass|Entity
 */
function entity_create_from_template($template, $parameters = array()) {
  if (is_string($template)) {
    $template = entity_load_single('entity_template', $template);
  }

  return $template->createNewEntityFromTemplate($parameters);
}

/**
 * Check whether a parameter name exists.
 */
function entity_template_check_param_machine_name() {
  return FALSE;
}

/**
 * Form wrapper for entity create form.
 *
 * Include the UI file for this form.
 */
function entity_template_entity_create_form_wrapper($form, &$form_state) {
  form_load_include($form_state, 'inc', 'entity_template', 'entity_template.ui');
  return $form;
}

/**
 * Get a list of all the access groups.
 */
function entity_template_access_groups() {
  return variable_get('entity_template_access_groups', array('standard' => t('Standard')));
}

/**
 * Implements hook_field_extra_fields().
 */
function entity_template_field_extra_fields() {
  $extra['entity_template']['entity_template'] = array(
    'form' => array(
      'label' => array(
        'label' => t('Label'),
        'description' => t('Template Label'),
        'weight' => -8,
      ),
      'entity_type' => array(
        'label' => t('Entity Type'),
        'description' => t('The type of entity this template creates.'),
        'weight' => -6,
      ),
      'bundle' => array(
        'label' => t('Bundle'),
        'description' => t('The bundle this template creates.'),
        'weight' => -4,
      ),
    ),
  );
  return $extra;
}

/**
 * Entity template tags options callback.
 */
function entity_template_tags_options() {
  $query = db_select('entity_template_tags', 't');
  $query->addField('t', 'tag', 'tag');
  $query->distinct();

  return drupal_map_assoc($query->execute()->fetchCol());
}

/**
 * Entity template tags autocomplete callback.
 */
function entity_template_tags_autocomplete_callback($string = '') {
  $tags_typed = drupal_explode_tags($string);
  $tag_last = drupal_strtolower(array_pop($tags_typed));

  if (!empty($tag_last)) {
    $prefix = count($tags_typed) ? implode(', ', $tags_typed) . ', ' : '';
  }

  $matches = array();
  if (isset($tag_last)) {
    $query = db_select('entity_template_tags', 't')
      ->fields('t', array('tag'));
    $query->condition('t.tag', '%'.db_like($string).'%', 'LIKE');
    $query->distinct();
    foreach ($query->execute()->fetchCol() as $tag) {
      $matches[$prefix.$tag] = $tag;
    }
  }

  drupal_json_output($matches);
}

/**
 * Get the component plugin info.
 *
 * @param string|null $plugin_name
 *   The plugin name to get info for.
 *
 * @return array|false
 *   Either a list of plugins or specific plugin info, or false of the specified
 *   plugin does not exist.
 */
function entity_template_component_plugin_info($plugin_name = NULL) {
  $plugins = &drupal_static(__FUNCTION__, array());
  if (!empty($plugins)) {
    return $plugin_name ? ($plugins[$plugin_name] ?? FALSE) : $plugins;
  }

  if ($cached = cache_get(__FUNCTION__)) {
    $plugins = $cached->data;
  }
  else {
    $plugins = module_invoke_all('entity_template_component_plugin_info');
    drupal_alter('entity_template_component_plugin_info', $plugins);

    cache_set(__FUNCTION__, $plugins);
  }

  return $plugin_name ? ($plugins[$plugin_name] ?? FALSE) : $plugins;
}

function entity_template_component_plugin_create_instance($plugin_name, TemplateConfiguration $template_configuration, $definition = [], $configuration = []) {
  $info = entity_template_component_plugin_info($plugin_name);

  return $info['class']::create($template_configuration, $plugin_name, $definition + $info, $configuration);
}
