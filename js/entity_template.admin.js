(function($) {
  Drupal.behaviors.entityTemplateAdminFieldsetSummaries = {
    attach: function(context) {
      $('fieldset.entity-template-property-tab', context).find(':input').change(function() {
        var filled = ($(this).val().length > 1);
        var key = $(this).closest('fieldset.entity-template-property-tab').attr('property');
        Drupal.settings.entity_template[key].filled = filled;
      });
      $('fieldset.entity-template-property-tab', context).drupalSetSummary(function(context) {
        var key = $(context).attr('property');
        var filled = Drupal.settings.entity_template[key].filled;
        var required = Drupal.settings.entity_template[key].required;
        var donotuse = Drupal.settings.entity_template[key].donotuse;
        var error = false;
        $(context).find('.error').each(function() {
          error = true;
        });
        var message = '';

        // Work out whether its required.
        if (donotuse) {
          message += "<span class=\"entity-template-property-frequency entity-template-property-rare\">"+Drupal.t('Rare')+"</span>";
        }
        else if (required) {
          message += "<span class=\"entity-template-property-frequency entity-template-property-frequent\">"+Drupal.t('Frequent')+"</span>";
        }
        else {
          message += "<span class=\"entity-template-property-frequency entity-template-property-uncommon\">"+Drupal.t('Uncommon')+"</span>";
        }

        // Work out whether its filled.
        if (error) {
          message += "<span class=\"entity-template-property-status entity-template-property-error\">"+Drupal.t('ERROR')+"</span>";
        }
        else if (filled) {
          message += "<span class=\"entity-template-property-status entity-template-property-in-use\">"+Drupal.t('Filled')+"</span>";
        }
        else {
          message += "<span class=\"entity-template-property-status entity-template-property-empty\">"+Drupal.t('Empty')+"</span>";
        }
        return message;
      });
    }
  };

  Drupal.behaviors.entityTemplateStickyVerticalTabs = {
    attach: function (context, settings) {
      $('.vertical-tabs').once('sticky-tabs', function() {
        $(window)
          .bind('scroll.sticky-tabs', Drupal.behaviors.entityTemplateStickyVerticalTabs.recalculate.bind(this))
          .bind('resize.sticky-tabs', Drupal.behaviors.entityTemplateStickyVerticalTabs.recalculate.bind(this))
          .triggerHandler('resize.sticky-tabs');
      });
    },
    recalculate: function (event) {
      var calculateWidth = event.data && event.data.calculateWidth;
      var stickyOffsetTop = 40;
      var tabsPane = $(this).find('.vertical-tabs-panes');
      tabsPane.css('top', stickyOffsetTop + 'px');
      tabsPane.css('width', tabsPane.width());

      // Save positioning data.
      var viewHeight = document.documentElement.scrollHeight || document.body.scrollHeight;
      var vPosition = $(this).offset().top - 4 - stickyOffsetTop;
      var hPosition = $(this).offset().left;
      var vLength = this.clientHeight - tabsPane.height();

      var hScroll = document.documentElement.scrollLeft || document.body.scrollLeft;
      var vOffset = (document.documentElement.scrollTop || document.body.scrollTop) - vPosition;

      var stickyVisible = vOffset > 0 && vOffset < vLength;
      tabsPane.css({ /*left: (-hScroll + hPosition) + 'px',*/ position: stickyVisible ? 'fixed' : 'static' });
    }
  };
})(jQuery);
