<?php
/**
 * @file
 *   UI Code for Creating Entities from Entity Templates.
 */

/**
 * Page callback for creating entity.
 */
function entity_template_create_entity_page($template = NULL, $js = FALSE) {
  if (!$js) {
    $form_state = array();
    $form_state['build_info']['args'] = array($template);
    $form = drupal_build_form('entity_template_entity_create_form', $form_state);

    if (!empty($form_state['redirect'])) {
      drupal_redirect_form($form_state);
    }

    return $form;
  }

  ctools_include('ajax');
  ctools_include('modal');

  $form_state = array(
    'ajax' => TRUE,
    'title' => entity_template_create_entity_page_title($template),
    'build_info' => array(
      'args' => array(
        $template,
      ),
    ),
  );
  $commands = ctools_modal_form_wrapper('entity_template_entity_create_form', $form_state);

  if (!empty($form_state['executed']) && empty($form_state['rebuild'])) {
    // The form has been executed, so let's redirect to the destination page.
    $commands = array();
    if (!empty($_GET['destination'])) {
      $commands[] = ctools_ajax_command_redirect($_GET['destination']);
    }
    elseif (!empty($form_state['redirect'])) {
      if (is_array($form_state['redirect'])) {
        $redirect = $form_state['redirect'][0];
        $options = (!empty($form_state['redirect'][1])) ? $form_state['redirect'][1] : array();
      }
      else {
        $redirect = $form_state['redirect'];
        $options = array();
      }
      $commands[] = ctools_ajax_command_redirect($redirect, $delay, $options);
    }
    else {
      $commands[] = ctools_modal_command_dismiss();
    }
  }

  ajax_deliver(array(
    '#type' => 'ajax',
    '#commands' => $commands,
  ));
  drupal_exit();
}

/**
 * Form Callback to create an entity from an entity template.
 *
 * Split into two/three create_steps:
 *   1) Choose a template if one is not selected.
 *   2) Enter parameters if not provided.
 *   3) Review and update entity.
 */
function entity_template_entity_create_form($form, &$form_state, $template = NULL, array $parameters = array()) {
  if (!empty($form_state['template'])) {
    $template = $form_state['template'];
  }
  $form_state['template'] = $template;

  if (!empty($form_state['parameters'])) {
    $parameters = $form_state['parameters'];
  }
  $form_state['parameters'] = $parameters;

  if (empty($template) && empty($form_state['initial_query'])) {
    $form_state['initial_query'] = $_GET;
  }

  $required_parameters = array();
  if (!empty($template->parameters)) {
    foreach ($template->parameters as $key => $info) {
      if (empty($info['optional'])) {
        $required_parameters[] = $key;
      }
      if (isset($_GET[$key])) {
        $parameters[$key] = $form_state['parameters'][$key] = $_GET[$key];
      }
      elseif (isset($form_state['initial_query'][$key])) {
        $parameters[$key] = $form_state['parameters'][$key] = $form_state['initial_query'][$key];
      }
    }
  }

  $submit = t('Continue');

  // create_step 1: If there is no template selected then present a form to choose one.
  if (empty($template)) {
    $form_state['create_step'] = 'template';
    $form['template'] = array(
      '#type' => 'select',
      '#title' => t('Template'),
      '#description' => t('Please select a template to use.'),
      '#options' => entity_template_entity_create_template_options(
        isset($form_state['initial_query']['__entity_type']) ? $form_state['initial_query']['__entity_type'] : NULL,
        isset($form_state['initial_query']['__bundle']) ? $form_state['initial_query']['__bundle'] : NULL,
        isset($form_state['initial_query']['__tag']) ? $form_state['initial_query']['__tag'] : NULL
      ),
      '#required' => TRUE,
    );
  }
  // create_step 2: If there are parameters and none are given.
  else if (
    !empty($template->parameters)
    && count(array_diff(array_keys($template->parameters), array_keys($parameters)))
    && ($form_state['create_step'] != 'parameters' || count(array_diff($required_parameters, array_keys($parameters))))
  ) {
    $form_state['create_step'] = 'parameters';
    $data_info = rules_fetch_data('data_info');
    $rules_plugin = rules_action('entity_template_create', array('entity_template' => $template->name));

    // We call the whole form to make sure the relevant includes get included.
    // @todo: Work out how not to build the whole form.
    $tmp_form = $form;
    $rules_plugin->form($tmp_form, $form_state);

    $form['parameters'] = array(
      '#type' => 'container',
      '#tree' => TRUE,
      '#parents' => array('parameters'),
    );

    foreach ($template->parameters as $name => $param) {
      // If we already have this parameter then don't show the form.
      if (!empty($parameters[$name])) {
        continue;
      }

      $class = $data_info[$param['type']]['ui class'];
      $form['parameters'] += $class::inputForm($name, $param, array(), $rules_plugin);
      $form['parameters'][$name]['#title'] = $param['label'];
    }

    drupal_alter('entity_template_create_form_parameter_step', $form['parameters'], $form_state, $template);
  }
  else {
    $form_state['create_step'] = 'entity';
    $form_state['entity'] = $entity = $template->createNewEntityFromTemplate($form_state['parameters']);

    if (empty($template->settings['form']) || $template->settings['form'] == 'entity_template:standard') {
      $entity_type = $template->entity_type;
      $bundle = $template->bundle;

      $form_id = (!isset($bundle) || $bundle == $entity_type) ? $entity_type . '_form' : $entity_type . '_edit_' . $bundle . '_form';

      // Set up a sandbox form_state.
      $form_state['build_info']['args'] = array(
        $entity,
        $op,
        $entity_type,
      );
      $form += drupal_retrieve_form($form_id, $form_state);
      drupal_prepare_form($form_id, $form, $form_state);

      return $form;
    }
    else if (substr($template->settings['form'], 0, 10) == 'flexiform:') {
      $flexiform_name = substr($template->settings['form'], 10);
      $flexiform = entity_load_single('flexiform', $flexiform_name);
      try {
        if ($flexiform) {
          $builder = $flexiform->getBuilder($entity);
          $form += $builder->form($form, $form_state);
        }
        else {
          drupal_set_message('The form required by this template cannot be loaded, please contact an administrator.', 'error');

          return array();
        }
      }
      catch (Exception $e) {
        watchdog_exception('entity_template', $e);
      }
    }
    else {
      // If there is no form then save the entity.
      entity_save($template->entity_type, $entity);
      $form_state['executed'] = TRUE;
      $form_state['rebuild'] = FALSE;
      entity_template_entity_create_form_redirect($form_state, $template, $entity);
    }
  }

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => $submit,
    '#validate' => array(
      'entity_template_entity_create_form_validate',
    ),
    '#submit' => array(
      'entity_template_entity_create_form_submit',
    ),
  );

  return $form;
}

/**
 * Work out the redirect information for the entity_template ui.
 */
function entity_template_entity_create_form_redirect(&$form_state, $template, $created_entity) {
  global $user;

  // Set up normal redirect.
  $redirect = array('', array());
  if (!empty($template->settings['redirect'])) {
    if ($template->settings['redirect'] == '<none>') {
      $form_state['redirect'] = FALSE;
      return;
    }

    ctools_include('context');
    $contexts = array(
      'site' => ctools_context_create('token'),
      'current-user' => ctools_context_create('entity:user', $user),
      'created-entity' => ctools_context_create('entity:'.$template->entity_type, $created_entity),
    );
    $contexts['current-user']->keyword = 'current-user';
    $contexts['current-user']->identifier = t('Logged-in user');
    $contexts['created-entity']->keyword = 'created-entity';
    $contexts['created-entity']->identifier = t('Created Entity');

    $redirect = array(
      ctools_context_keyword_substitute($template->settings['redirect'], array(), $contexts),
      array(),
    );
  }

  drupal_alter('entity_template_entity_create_form_redirect', $redirect, $template, $created_entity);
  $form_state['redirect'] = $redirect;
}

/**
 * Form validate callback for the entity template entity create form.
 */
function entity_template_entity_create_form_validate($form, &$form_state) {
  if ($form_state['create_step'] == 'entity' && !empty($form['#flexiform_builder'])) {
    $form['#flexiform_builder']->formValidate($form, $form_state);
  }
}

/**
 * Form submit callback for the entity template entity create form.
 */
function entity_template_entity_create_form_submit($form, &$form_state) {
  // create_step 1 Submission: Template Select
  if (empty($form_state['template'])) {
    $template_name = $form_state['values']['template'];
    $form_state['template'] = entity_load_single('entity_template', $template_name);
    $form_state['rebuild'] = TRUE;
  }
  else if ($form_state['create_step'] == 'parameters') {
    foreach ($form_state['template']->parameters as $name => $info) {
      if (empty($form_state['values']['parameters'][$name])) {
        continue;
      }

      if ($info['type'] === 'date' && !is_numeric($form_state['values']['parameters'][$name])) {
        $form_state['values']['parameters'][$name] = RulesDateInputEvaluator::gmstrtotime($form_state['values']['parameters'][$name]);
      }

      $form_state['parameters'][$name] = entity_metadata_wrapper(
        $info['type'],
        $form_state['values']['parameters'][$name]
      )->value();
    }

    if (empty($template->settings['form']) || $template->settings['form'] == 'entity_template:none') {
      $form_state['rebuild'] = TRUE;
    }
  }
  else if ($form_state['create_step'] == 'entity') {
    if (!empty($form['#flexiform_builder'])) {
      $form['#flexiform_builder']->formSubmit($form, $form_state);
    }

    $template = $form_state['template'];

    if ($form['#flexiform_builder']) {
      $created_entity = $form['#flexiform_builder']->getEntityManager()->getBaseEntity();
      $form_state['entity'] = $created_entity;
    }
    else if (!empty($form_state['entity'])) {
      $created_entity = $form_state['entity'];
    }

    // If there is no form then save the entity.
    if ($template->settings['form'] == 'entity_template:none') {
      entity_save($template->entity_type, $created_entity);
    }

    entity_template_entity_create_form_redirect($form_state, $template, $created_entity);
  }
}

/**
 * Get options for templates.
 */
function entity_template_entity_create_template_options($entity_type = NULL, $bundle = NULL, $tag = NULL) {
  $q = db_select('entity_template', 't')
    ->fields('t', array('name', 'label', 'entity_type', 'bundle'));
  if (!empty($entity_type)) {
    $q->condition('entity_type', $entity_type);
  }
  if (!empty($bundle)) {
    $q->condition('bundle', $bundle);
  }
  if (!empty($tag)) {
    $q->innerJoin('entity_template_tags', 'tt', 'tt.id = t.id AND tt.vid = t.vid AND tt.tag = :tag', array(':tag' => $tag));
    $q->groupBy('t.name');
  }

  $templates = $q->execute()->fetchAllAssoc('name');

  $info = entity_get_info();
  $options = array();
  foreach ($templates as $name => $template) {
    if (empty($entity_type)) {
      $options[$info[$template->entity_type]['label']][$name] = $template->label;
    }
    else if (empty($bundle)) {
      $options[$info[$template->entity_type]['bundles'][$template->bundle]['label']][$name] = $template->label;
    }
    else {
      $options[$name] = $template->label;
    }
  }

  return $options;
}
