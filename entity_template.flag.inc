<?php
/**
 * @file
 * Flag integration.
 */

/**
 * Implements hook_flag_type_info().
 */
function entity_template_flag_type_info() {
  $items = array();
  $items['entity_template'] = array(
    'title' => t('Templates'),
    'description' => t('Entity Templates'),
    'handler' => 'flag_entity',
  );
  return $items;
}

