<?php
/**
 * @file
 *  Entity Template Metadata Info.
 */

/**
 * Implements hook_entity_property_info_alter().
 */
function entity_template_entity_property_info_alter(&$property_info) {
  $properties =& $property_info['entity_template']['properties'];

  $properties['entity_type']['options list'] = 'entity_template_entity_type_options_list';
  $properties['bundle']['options list'] = 'entity_template_bundle_options_list';
  $properties['status']['options list'] = 'entity_template_status_options_list';

  $properties['log'] = array(
    'type' => 'text',
    'label' => t('Log Message'),
    'description' => t('The Log Message Stored with the Revision'),
    'getter callback' => 'entity_property_verbatim_get',
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['user'] = array(
    'type' => 'user',
    'label' => t('User'),
    'schema field' => 'uid',
    'description' => t('The last user to save this template'),
    'getter callback' => 'entity_property_verbatim_get',
  );
}
