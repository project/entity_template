Entity Template module allows you to store templates for frequently created
similar entities.

Entity Template supports any Entity API entity. For the forms on the interface
the module makes extensive use for the Entity API UI Controllers, so please
ensure that your entity follows those standards of form naming etc.

Sometimes Entity Template cannot find the relevant form validation or form
submission handlers, resulting in a 500 Error. If this happens include the
file containing these handlers in the $form_state['build_info'] (for example
using form_load_include()).
