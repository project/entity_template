<?php
/**
 * @file
 *  Contains class for a entity getter from entity template.
 */

/**
 * Form entity that creates a new entity.
 */
class EntityTemplateFormEntityCreateFromTemplate extends FlexiformFormEntityBase {

  /**
   * {@inheritdoc}
   */
  public function getEntity() {
    parent::getEntity();

    $template = entity_load_single('entity_template', $this->getter['entity_template']);
    $parameters = array();

    foreach ($template->parameters as $name => $parameter) {
      if (isset($this->getter['params'][$name])) {
        $parameters[$name] = $this->getParam($name);
      }
      else {
        $parameters[$name] = isset($settings[$name]) ? $settings[$name] : NULL;
      }
    }

    $entity = $template->createNewEntityFromTemplate($parameters);
    return $this->checkBundle($entity) ? $entity : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function configForm($form, &$form_state) {
    $form = parent::configForm($form, $form_state);

    $template = entity_load_single('entity_template', $this->getter['entity_template']);
    $data_info = rules_fetch_data('data_info');
    $rules_plugin = rules_action('entity_template_create', array('entity_template' => $template->name));

    // We call the whole form to make sure the relevant includes get included.
    // @todo: Work out how not to build the whole form.
    $tmp_form = $form;
    $rules_plugin->form($tmp_form, $form_state);
    foreach ($template->parameters as $name => $parameter) {
      if (isset($this->getter['params'][$name])) {
        continue;
      }

      $class = $data_info[$parameter['type']]['ui class'];
      $form['settings'] += $class::inputForm($name, $parameter, array(), $rules_plugin);
      $form['settings'][$name]['#title'] = $parameter['label'];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configFormSubmit($form, &$form_state) {
    $template = entity_load_single('entity_template', $this->getter['entity_template']);
    foreach ($template->parameters as $name => $parameter) {
      if (empty($form_state['values']['settings'][$name])) {
        continue;
      }

      $wrapper = entity_metadata_wrapper($parameter['type'], $form_state['values']['settings'][$name]);
      if ($wrapper instanceof EntityDrupalWrapper) {
        $form_state['values']['settings'][$name] = $wrapper->getIdentifier();
      }
      else {
        $form_state['values']['settings'][$name] = $wrapper->value();
      }
    }

    parent::configFormSubmit($form, $form_state);
  }
}
