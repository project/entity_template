<?php

/**
 * Component plugin to input a value.
 */
class InputValueEntityTemplateComponent extends EntityTemplateComponentBase implements EntityTemplateComponentRequiringSetupInterface, PropertySettingEntityComponentInterface, EntityTemplateSwappableComponentInterface {
  use PropertySettingEntityTemplateComponentTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(
    TemplateConfiguration $template_config,
    string $plugin_id,
    array $definition,
    array $configuration
  ): EntityTemplateComponentInterface {
    if ($definition['property']) {
      /** @var \EntityStructureWrapper $structure */
      $structure = $template_config->getStructure();
      $property_info = $structure->getPropertyInfo($definition['property']);
      $candidate_classes = array();

      if (!empty($property_info['field'])) {
        $field = field_info_field($definition['property']);
        $candidate_classes[] = 'InputValueEntityTemplateComponent__FieldName__' . $definition['property'];
        $candidate_classes[] = 'InputValueEntityTemplateComponent__FieldType__' . $field['type'];
        $candidate_classes[] = 'InputValueEntityTemplateComponent__Field';
      }
      else {
        $property = $structure->{$definition['property']};
        $property_type = $property instanceof EntityListWrapper ?
          'List__' . entity_property_list_extract_type($property->type()) :
          $property->type();

        $candidate_classes[] = 'InputValueEntityTemplateComponent__Property__' . $structure->type() . '__' . $definition['property'];
        $candidate_classes[] = 'InputValueEntityTemplateComponent__PropertyType__' . $property_type;
        if ($property instanceof EntityListWrapper) {
          $candidate_classes[] = 'InputValueEntityTemplateComponent__PropertyType__List';
        }
        $candidate_classes[] = 'InputValueEntityTemplateComponent__Property';
      }

      foreach ($candidate_classes as $candidate_class) {
        if (class_exists($candidate_class) && is_subclass_of($candidate_class, static::class)) {
          return new $candidate_class($template_config, $plugin_id, $definition, $configuration);
        }
      }
    }

    return parent::create($template_config, $plugin_id, $definition, $configuration);
  }

  /**
   * {@inheritdoc}
   */
  protected function inputMethodLabel(): string {
    return t('Directly input');
  }

  /**
   * {@inheritdoc}
   */
  public function apply(EntityMetadataWrapper $structure, array $parameters = array()) {
    $value = $this->applyModifiers($this->configuration['value'], $parameters);

    if (
      entity_property_list_extract_type($structure->{$this->definition['property']}->type()) &&
      !empty($this->configuration['list_behavior'])
    ) {
      switch ($this->configuration['list_behavior']) {
        case 'prepend':
          $structure->{$this->definition['property']} = array_merge(
            is_array($value) ? $value : [$value],
            $structure->{$this->definition['property']}->value()
          );
          break;
        case 'append':
          $structure->{$this->definition['property']} = array_merge(
            $structure->{$this->definition['property']}->value(),
            is_array($value) ? $value : [$value]
          );
          break;
        case 'replace':
        default:
          $structure->{$this->definition['property']} = $value;
          break;
      }
    }
    else if (
      $structure->{$this->definition['property']}->type() === 'text' &&
      !empty($this->configuration['text_behavior'])
    ) {
      switch ($this->configuration['text_behavior']) {
        case 'prepend':
          $structure->{$this->definition['property']} = $value . $structure->{$this->definition['property']}->value();
          break;
        case 'append':
          $structure->{$this->definition['property']} = $structure->{$this->definition['property']}->value() . $value;
          break;
        case 'replace':
        default:
          $structure->{$this->definition['property']} = $value;
          break;
      }
    }
    else if (
      $structure->{$this->definition['property']}->type() === 'text_formatted' &&
      !empty($this->configuration['text_behavior'])
    ) {
      switch ($this->configuration['text_behavior']) {
        case 'prepend':
          $structure->{$this->definition['property']} = [
            'value' => $value['value'] . $structure->{$this->definition['property']}->value->value(),
            'format' => $structure->{$this->definition['property']}->format->value() ?: $value['format'],
          ];
          break;
        case 'append':
          $structure->{$this->definition['property']} = [
            'value' => $structure->{$this->definition['property']}->value->value() . $value['value'],
            'format' => $structure->{$this->definition['property']}->format->value() ?: $value['format'],
          ];
          break;
        case 'replace':
        default:
          $structure->{$this->definition['property']} = $value;
          break;
      }
    }
    else if (
      $structure->{$this->definition['property']}->type() === 'date' &&
      is_string($value)
    ) {
      $structure->{$this->definition['property']} = strtotime($value);
    }
    else {
      $structure->{$this->definition['property']} = $value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function configForm($form, &$form_state) {
    $form = parent::configForm($form, $form_state);
    $structure = $this->templateConfig->getStructure();

    $target_property_info = $structure->getPropertyInfo($this->definition['property']);
    if (entity_property_list_extract_type($target_property_info['type'])) {
      $form['list_behavior'] = [
        '#type' => 'select',
        '#title' => t('List Behaviour'),
        '#options' => [
          'prepend' => t('Add to the start of the list.'),
          'replace' => t('Replace the list.'),
          'append' => t('Add to the end of the list.'),
        ],
        '#default_value' => $this->configuration['list_behavior'] ?? 'replace',
      ];
    }

    if (in_array($target_property_info['type'], ['text', 'text_formatted']) && empty($target_property_info['options list'])) {
      $form['text_behavior'] = [
        '#type' => 'select',
        '#title' => t('Text Behavior'),
        '#options' => [
          'prepend' => t('Add to the start of the text.'),
          'replace' => t('Set the text.'),
          'append' => t('Add to the end of the text.'),
        ],
        '#default_value' => $this->configuration['text_behavior'] ?? 'replace',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configFormSubmit($form, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents'] ?? []);
    $this->configuration['list_behavior'] = $values['list_behavior'];
    $this->configuration['text_behavior'] = $values['text_behavior'];

    parent::configFormSubmit($form, $form_state);
  }

}
