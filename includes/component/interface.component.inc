<?php

/**
 * Interface for entity template components.
 */
interface EntityTemplateComponentInterface {

  /**
   * Create an instance of the plugin.
   *
   * @param \TemplateConfiguration $template_config
   *   The template configuration this is part of.
   * @param string $plugin_id
   *   The plugin id.
   * @param array $definition
   *   The definition of the plugin.
   * @param array $configuration
   *   The configuration of the plugin.
   *
   * @return \EntityTemplateComponentInterface
   *   The component plugin instance.
   */
  public static function create(TemplateConfiguration $template_config, string $plugin_id, array $definition, array $configuration) : EntityTemplateComponentInterface;

  /**
   * Get the plugin definition.
   *
   * @return array
   */
  public function getDefinition() : array;

  /**
   * Get the plugin id.
   *
   * @return string
   */
  public function getPluginId() : string;

  /**
   * Suggest a config key.
   *
   * @return string
   */
  public function suggestConfigKey() : string;

  /**
   * Get the label of this component.
   *
   * @return string
   *   The label of the component.
   */
  public function label() : string;

  /**
   * Get the description of the component.
   *
   * @return string
   *   The description of the component.
   */
  public function description() : string;

  public function configForm($form, &$form_state);

  public function configFormValidate($form, &$form_state);

  public function configFormSubmit($form, &$form_state);

  /**
   * Apply this component to the structure.
   *
   * @param \EntityMetadataWrapper $structure
   *
   * @return mixed
   */
  public function apply(EntityMetadataWrapper $structure, array $parameters = array());

  /**
   * Test whether this component can apply to the template or structure.
   *
   * @param \TemplateConfiguration $template_configuration
   *   The template configuration.
   *
   * @return bool
   *   TRUE if the template can apply, false otherwise.
   */
  public static function applies(TemplateConfiguration $template_configuration) : bool;

}

/**
 * Interface for a component that needs to be setup before it can be used.
 */
interface EntityTemplateComponentRequiringSetupInterface extends EntityTemplateComponentInterface {

  public function needsSetup() : bool;

  public function setupForm($form, &$form_state);

  public function setupFormValidate($form, &$form_state);

  public function setupFormSubmit($form, &$form_state);
}
