<?php

/**
 * Interface for components that set properties.
 */
interface PropertySettingEntityComponentInterface {

  /**
   * Get the target property definition.
   *
   * @return array
   */
  public function getTargetPropertyDefinition() : array;

}
