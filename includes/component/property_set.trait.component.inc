<?php

trait PropertySettingEntityTemplateComponentTrait {

  /**
   * {@inheritdoc}
   */
  public function suggestConfigKey(): string {
    return $this->definition['property'];
  }

  /**
   * {@inheritdoc}
   */
  public function label() : string {
    $property = $this->definition['property'];

    $structure = $this->templateConfig->getStructure();
    if (
      $structure instanceof EntityStructureWrapper &&
      ($info = $structure->getPropertyInfo($property))
    ) {
      return $info['label'] ?? $property;
    }

    return t(
      'Invalid of missing property "@property".',
      array('@property' => $property)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function description() : string {
    $property = $this->definition['property'];

    $structure = $this->templateConfig->getStructure();
    if (
      $structure instanceof EntityStructureWrapper &&
      ($info = $structure->getPropertyInfo($property))
    ) {
      return t('@method value for @property', array(
        '@method' => $this->inputMethodLabel(),
        '@property' => $info['label'] ?? $property,
      ));
    }

    return t(
      'Invalid of missing property "@property".',
      array('@property' => $property)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function needsSetup() : bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setupForm($form, &$form_state) {
    /** @var \EntityStructureWrapper $structure */
    $structure = $this->templateConfig->getStructure();

    if (!isset($form_state['definition'])) {
      $form_state['definition'] = array();
    }

    $form['property'] = array(
      '#type' => 'select',
      '#title' => t('Property'),
      '#required' => TRUE,
      '#options' => $this->applicableProperties($structure),
      '#default_value' => $form_state['definition']['property'] ?? ($this->definition['property'] ?? NULL),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setupFormValidate($form, &$form_state) {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function setupFormSubmit($form, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    $this->definition['property'] = $values['property'];
  }


  /**
   * Get the target property definition.
   *
   * @return array
   */
  public function getTargetPropertyDefinition() : array {
    return $this->templateConfig->getStructure()->getPropertyInfo($this->definition['property']);
  }

  /**
   * {@inheritdoc}
   */
  public static function applies(TemplateConfiguration $template_configuration): bool {
    return $template_configuration->getStructure() instanceof EntityStructureWrapper;
  }

  /**
   * Get the properties this can be applied to.
   *
   * @param \EntityStructureWrapper $structure
   *   The structure.
   *
   * @return array
   */
  protected function applicableProperties(EntityStructureWrapper $structure) {
    $props = [];

    foreach ($structure->getPropertyInfo() as $name => $info) {
      if (!empty($info['computed']) || empty($info['setter callback'])) {
        continue;
      }

      $props[$name] = $info['label'];
    }

    return $props;
  }

  /**
   * Can this swap for the given component.
   *
   * @param \EntityTemplateComponentInterface $component
   *   The component to be swapped out.
   *
   * @return bool
   */
  public function canSwapFor(EntityTemplateComponentInterface $component) : bool {
    if (
      $component instanceof PropertySettingEntityComponentInterface
      && !empty($component->getDefinition()['property'])
    ) {
      $app_properties = $this->applicableProperties($this->templateConfig->getStructure());
      return isset($app_properties[$component->getDefinition()['property']]);
    }

    return FALSE;
  }

  /**
   * Prepare the swap definition for this component.
   *
   * @param \EntityTemplateComponentInterface $component
   *   The component being replaced
   *
   * @return array
   */
  public function prepareSwapDefinitionFor(EntityTemplateComponentInterface $component) : array {
    return [
      'property' => $component->getDefinition()['property'],
    ];
  }

  /**
   * Prepare the swap configuration for this component.
   *
   * @param \EntityTemplateComponentInterface $component
   *   The component being swapped.
   *
   * @return array
   */
  public function prepareSwapConfigurationFor(EntityTemplateComponentInterface $component) : array {
    return [];
  }

  /**
   * Get the input method label.
   *
   * @return string
   */
  abstract protected function inputMethodLabel() : string;


}
