<?php

/**
 * Base class for entity template components.
 */
abstract class EntityTemplateComponentBase implements EntityTemplateComponentInterface {

  /**
   * The plugin id.
   *
   * @var string
   */
  protected string $pluginId;

  /**
   * The definition of the component.
   *
   * @var array
   */
  protected array $definition = [];

  /**
   * The component configuration.
   *
   * @var array
   */
  protected array $configuration = [];

  /**
   * The template configuration.
   *
   * @var \TemplateConfiguration
   */
  protected TemplateConfiguration $templateConfig;

  /**
   * {@inheritdoc}
   */
  public static function create(TemplateConfiguration $template_config, string $plugin_id, array $definition, array $configuration): EntityTemplateComponentInterface {
    return new static($template_config, $plugin_id, $definition, $configuration);
  }

  /**
   * Construct a component.
   *
   * @param array $definition
   *   The definition.
   * @param array $configuration
   *   The configuration.
   */
  public function __construct(TemplateConfiguration $template_config, string $plugin_id, array $definition = array(), array $configuration = array()) {
    $this->pluginId = $plugin_id;
    $this->definition = $definition;
    $this->configuration = $configuration;
    $this->templateConfig = $template_config;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): string {
    return $this->pluginId;
  }

  /**
   * Get the definition of this plugin.
   *
   * @return array
   */
  public function getDefinition(): array {
    return $this->definition;
  }

  /**
   * Get the configuration of the plugin.
   *
   * @return array
   */
  public function getConfiguration(): array {
    return $this->configuration ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function configForm($form, &$form_state) {
    $form['#component_form_root'] = $this->templateConfig->getId() . '.' . end($form['#parents']);

    $mod_table_id = implode('--', array_merge($form['#parents'] ?? [], ['modifiers-table']));
    $mod_wrapper_id = implode('--', array_merge($form['#parents'] ?? [], ['modifiers-wrapper']));
    $component_state = static::getComponentState($form, $form_state);

    $form['modifiers'] = [
      '#type' => 'container',
      '#weight' => 50,
      '#attributes' => [
        'id' => $mod_wrapper_id,
      ],
      '#access' => EntityTemplateComponentModifierFactory::componentHasModifiers($this),
    ];
    $form['modifiers']['table'] = [
      '#theme' => 'table',
      '#header' => [
        t('Modifier'),
        t('Weight'),
      ],
      '#empty' => t('No modifiers are configured for this plugin.'),
      '#rows' => [],
      '#pre_render' => [
        'ck_opencrm_ui_form_table_pre_render',
      ],
      '#attributes' => [
        'id' => $mod_table_id,
      ],
    ];

    $component_state['modifiers'] = $component_state['modifiers'] ?? $this->configuration['modifiers'] ?? [];
    static::setComponentState($form, $form_state, $component_state);
    foreach ($component_state['modifiers'] as $delta => $modifier) {
      $instance = EntityTemplateComponentModifierFactory::createInstance($modifier['id'], $modifier)
        ->setComponent($this);
      if ($instance instanceof ParameterAwareEntityTemplateComponentModifierInterface) {
        $instance->setParameters(
          array_map(fn($pi) => entity_metadata_wrapper($pi['type'], NULL, $pi), $this->templateConfig->getParametersInfo())
        );
      }

      $form['modifiers']['table'][$delta] = [
        '#row_class' => ['draggable'],
        '#weight' => $modifier['weight'],
      ];

      $form['modifiers']['table'][$delta]['configuration'] = [
        'title' => [
          '#markup' => '<strong>' . $instance->getDefinition()['label'] . '</strong>',
        ],
        'description' => [
          '#markup' => '<p>' . ($instance->getDefinition()['description'] ?? '') .'</p>',
          '#access' => !empty($instance->getDefinition()['description']),
        ],
      ];

      if ($instance instanceof ConfigurableEntityTemplateComponentModifierInterface) {
        $form['modifiers']['table'][$delta]['configuration']['configuration'] = [
          '#type' => 'container',
          '#parents' => array_merge($form['#parents'] ?? [], ['modifiers', $delta, 'configuration']),
        ];
        $form['modifiers']['table'][$delta]['configuration']['configuration'] = $instance->configurationForm(
          $form['modifiers']['table'][$delta]['configuration']['configuration'],
          $form_state
        );
      }

      $form['modifiers']['table'][$delta]['configuration']['actions'] = [
        '#type' => 'actions',
        'remove' => [
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#modifier_delta' => $delta,
          '#name' => implode('__', $form['#parents'] ?? []) . '__modifiers__' . $delta . '__remove',
          '#component' => $form['#component_form_root'],
          '#limit_validation_errors' => [],
          '#validate' => [],
          '#submit' => [
            [$this, 'configFormSubmitRemoveModifier'],
          ],
          '#ajax' => [
            'wrapper' => $mod_wrapper_id,
            'callback' => [static::class, 'configFormAjaxRefreshModifierTable'],
          ],
          '#attributes' => [
            'class' => ['btn-red'],
          ],
        ],
      ];

      $form['modifiers']['table'][$delta]['weight'] = [
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => $modifier['weight'],
        '#delta' => 5,
        '#title_display' => 'invisible',
        '#parents' => array_merge($form['#parents'] ?? [], ['modifiers', $delta, 'weight']),
        '#attributes' => [
          'class' => ['modifier-weight'],
        ],
      ];
    }

    // Add table row for adding a component.
    $available_modifiers = [];
    foreach (EntityTemplateComponentModifierFactory::getDefinitions() as $id => $definition) {
      $instance = EntityTemplateComponentModifierFactory::createInstance($id, []);
      if (
        $instance->canModify($this) &&
        (
          empty($definition['singleton']) ||
          empty(array_filter($component_state['modifiers'], fn($conf) => $conf['id'] === $id))
        )
      ) {
        $available_modifiers[$id] = $definition['label'];
      }
    }

    if ($available_modifiers) {
      $form['modifiers']['table']['__new'] = [
        '#row_class' => ['draggable'],
        '#weight' => ($modifier['weight'] ?? 0) + 1,
        'configuration' => [
          '#parents' => array_merge($form['#parents'] ?? [], ['modifiers', '__new', 'configuration']),
          'modifier' => [
            '#type' => 'select',
            '#options' => $available_modifiers,
          ],
          'add_modifier' => [
            '#type' => 'submit',
            '#value' => t('Add Modifier'),
            '#name' => implode('__', array_merge($form['#parents'] ?? [], ['modifiers', '__new', 'add'])),
            '#limit_validation_errors' => [
              array_merge($form['#parents'] ?? [], ['modifiers', '__new', 'configuration']),
            ],
            '#component' => $form['#component_form_root'],
            '#validate' => [],
            '#submit' => [
              [$this, 'configFormSubmitAddModifier'],
            ],
            '#ajax' => [
              'wrapper' => $mod_wrapper_id,
              'callback' => [static::class, 'configFormAjaxRefreshModifierTable'],
            ],
          ],
        ],
        'weight' => [
          '#type' => 'weight',
          '#title' => t('Weight'),
          '#default_value' => ($modifier['weight'] ?? 0) + 1,
          '#delta' => 5,
          '#title_display' => 'invisible',
          '#parents' => array_merge($form['#parents'] ?? [], ['modifiers', '__new', 'weight']),
          '#attributes' => [
            'class' => ['modifier-weight'],
          ],
        ]
      ];
    }

    drupal_add_tabledrag(
      $mod_table_id,
      'order',
      'sibling',
      'modifier-weight',
      NULL,
      NULL
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configFormValidate($form, &$form_state) {
    $component_state = static::getComponentState($form, $form_state);
    foreach ($component_state['modifiers'] as $delta => $modifier) {
      $instance = EntityTemplateComponentModifierFactory::createInstance($modifier['id'], $modifier)
        ->setComponent($this);
      if ($instance instanceof ConfigurableEntityTemplateComponentModifierInterface) {
        $instance->configurationFormValidate(
          $form['modifiers']['table'][$delta]['configuration']['configuration'],
          $form_state
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function configFormSubmit($form, &$form_state) {
    $component_state = static::getComponentState($form, $form_state);
    $this->configuration['modifiers'] = [];
    foreach ($component_state['modifiers'] as $delta => $modifier) {
      $instance = EntityTemplateComponentModifierFactory::createInstance($modifier['id'], $modifier)
        ->setComponent($this);
      if ($instance instanceof ConfigurableEntityTemplateComponentModifierInterface) {
        $instance->configurationFormSubmit(
          $form['modifiers']['table'][$delta]['configuration']['configuration'],
          $form_state
        );

        $this->configuration['modifiers'][$delta] = [
            'id' => $modifier['id'],
          ] + $instance->getConfiguration();
      }
      else {
        $this->configuration['modifiers'][$delta] = [
          'id' => $modifier['id'],
        ];
      }
      $this->configuration['modifiers'][$delta]['weight'] = drupal_array_get_nested_value(
        $form_state['values'],
        array_merge($form['#parents'] ?? [], ['modifiers', $delta, 'weight'])
      );
    }
  }

  /**
   * Submit to remove a modifier.
   */
  public function configFormSubmitRemoveModifier($form, &$form_state) {
    $button = $form_state['triggering_element'];
    $com_form = static::getComponentForm($form, $button);
    $component_state = static::getComponentState($com_form, $form_state);
    unset($component_state['modifiers'][$button['#modifier_delta']]);
    static::setComponentState($com_form, $form_state, $component_state);
    $form_state['rebuild'] = TRUE;
  }

  /**
   * Submit to add a modifier.
   *
   * @param $form
   * @param $form_state
   *
   * @return void
   */
  public function configFormSubmitAddModifier($form, &$form_state) {
    $component_form = static::getComponentForm($form, $form_state['triggering_element']);
    $component_state = static::getComponentState($component_form, $form_state);

    $values = drupal_array_get_nested_value($form_state['values'], $component_form['#parents']);
    $plugin_id = $values['modifiers']['__new']['configuration']['modifier'];
    $key = $plugin_id; $d = 1;
    while (isset($component_state['modifiers'][$key])) {
      $key = $plugin_id . '_' . $d;
      $d++;
    }

    $component_state['modifiers'][$key] = [
      'id' => $plugin_id,
      'weight' => $values['modifiers']['__new']['weight'],
    ];
    static::setComponentState($component_form, $form_state, $component_state);

    $form_state['rebuild'] = TRUE;
  }

  /**
   * Ajax callback to refresh to modifier table.
   *
   * @param array $form
   *   The form.
   * @param array $form_state
   *   The form state.
   *
   * @return array
   */
  public static function configFormAjaxRefreshModifierTable($form, $form_state) {
    if ($component_form = static::getComponentForm($form, $form_state['triggering_element'])) {
      return $component_form['modifiers'];
    }

    return $form;
  }

  /**
   * Get the component form from an element.
   *
   * @param array $form
   * @param array $element
   *
   * @return array|mixed
   */
  protected static function getComponentForm($form, $element) {
    if (
      !empty($element['#component_form_root']) &&
      (
        empty($element['#component']) ||
        $element['#component'] === $element['#component_form_root']
      )
    ) {
      return $element;
    }

    $array_parents = $element['#array_parents'];
    $trigger = $element;
    do {
      if (
        ($element = drupal_array_get_nested_value($form, $array_parents)) &&
        (
          (!empty($trigger['#component']) && $trigger['#component'] === $element['#component_form_root']) ||
          (empty($trigger['#component']) && !empty($element['#component_form_root']))
        )
      ) {
        return $element;
      }
    } while($array_parents = array_slice($array_parents, 0, -1));

    return $form;
  }

  /**
   * Get the component state from an element.
   *
   * @param array $component_form
   *   The component form.
   * @param array $form_state
   *   The form state.
   *
   * @return array
   */
  protected static function &getComponentState($component_form, &$form_state) : array {
    $component_form = static::getComponentForm($form_state['complete form'] ?? [], $component_form);

    if (!isset($form_state['component_state'])) {
      $form_state['component_state'] = [];
    }

    $addr = $component_form['#parents'];
    $addr[] = '#state';
    $cs = &drupal_array_get_nested_value($form_state['component_state'], $addr, $ke);

    if (!$ke) {
      drupal_array_set_nested_value($form_state['component_state'], $addr, []);
      $cs = &drupal_array_get_nested_value($form_state['component_state'], $addr);
    }

    return $cs;
  }

  /**
   * Set the component state for an element.
   *
   * @param array $component_form
   * @param array $form_state
   * @param array $component_state
   *
   * @return void
   */
  protected static function setComponentState($component_form, &$form_state, $component_state) {
    if (!isset($form_state['component_state'])) {
      $form_state['component_state'] = [];
    }

    drupal_array_set_nested_value($form_state['component_state'], array_merge($component_form['#parents'], ['#state']), $component_state);
  }

  /**
   * Apply modifiers to the value.
   *
   * @param mixed $value
   *   The value.
   * @param array $parameters
   *   The parameters.
   *
   * @return mixed
   *   The modified value.
   */
  protected function applyModifiers($value, array $parameters = []) {
    $modifiers = $this->configuration['modifiers'] ?? [];
    usort($modifiers, 'drupal_sort_weight');
    foreach ($modifiers as $config) {
      $instance = EntityTemplateComponentModifierFactory::createInstance($config['id'], $config)
        ->setComponent($this);

      if ($instance instanceof ParameterAwareEntityTemplateComponentModifierInterface) {
        $instance->setParameters($parameters);
      }

      $value = $instance->modify($value);
    }

    return $value;
  }

}
