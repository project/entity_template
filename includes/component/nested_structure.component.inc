<?php

/**
 * Component for editing nested structures.
 */
class NestedStructureTemplateEntityTemplateComponent extends EntityTemplateComponentBase implements EntityTemplateComponentRequiringSetupInterface, PropertySettingEntityComponentInterface, EntityTemplateSwappableComponentInterface {
  use PropertySettingEntityTemplateComponentTrait {
    setupForm as traitSetupForm;
    setupFormSubmit as traitSetupFormSubmit;
  }
  use TemplateComponentsFormTrait;

  /**
   * {@inheritdoc}
   */
  public function apply(EntityMetadataWrapper $structure, array $parameters = []) {
    $property_info = $structure->getPropertyInfo($this->definition['property']);
    $config = entity_get_info($property_info['type']) ?
      new EntityTemplateConfiguration(
       $this->suggestConfigKey(),
       $property_info['type'],
       $this->definition['target_bundle'] ?? $property_info['type'],
       $this->configuration['components'] ?? [],
       [],
       $this->templateConfig
      ) :
      new StructureTemplateConfiguration(
        $this->suggestConfigKey(),
        $property_info,
        $this->configuration['components'] ?? [],
        [],
        $this->templateConfig,
      );

    /** @var \EntityStructureWrapper $property_structure */
    $property_structure = $structure->{$this->definition['property']};
    if ($config instanceof EntityTemplateConfiguration) {
      $values = [];
      if (!empty(entity_get_info($config->getEntityType())['entity keys']['bundle'])) {
        $values[entity_get_info($config->getEntityType())['entity keys']['bundle']] = $config->getBundle();
      }
      $property_structure->set(entity_create($config->getEntityType(), $values));
    }

    foreach ($config->getComponents(TRUE) as $id => $component) {
      $component->execute($property_structure, $parameters);
    }

    try {
      $modified = $this->applyModifiers($property_structure->value(), $parameters);
      if (is_null($modified)) {
        $property_structure->clear();
      }
      else {
        $property_structure->set($modified);
      }
    }
    catch (\EntityMetadataWrapperException $exception) {
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function applicableProperties(EntityStructureWrapper $structure) {
    $props = [];

    foreach ($structure->getPropertyInfo() as $name => $info) {
      if (!empty($info['computed']) || empty($info['setter callback'])) {
        continue;
      }

      if (empty($info['property info']) && empty($info['property info alter']) && !entity_get_info($info['type'])) {
        continue;
      }

      $props[$name] = $info['label'];
    }

    return $props;
  }

  /**
   * {@inheritdoc}
   */
  public function setupForm($form, &$form_state) {
    $form['#component_form_root'] = $this->templateConfig->getId() . '.' . end($form['#parents']);

    $actions_base_name = implode('__', $form['#parents'] ?? []) . '__setup_action__';
    $form = $this->traitSetupForm($form, $form_state);

    $form['update_property'] = [
      '#type' => 'submit',
      '#value' => t('Update Property'),
      '#name' => $actions_base_name . '__update_property',
      '#component_id' => '__add_component',
      '#attributes' => [
        'class' => ['js-hide'],
      ],
      '#submit' => [
        [$this, 'setupFormSubmitUpdateProperty'],
      ],
      '#ajax' => [
        'callback' => [static::class, 'setupFormSubmitRefreshAjaxCallback'],
        'wrapper' => $form['#attributes']['id'],
        'method' => 'replace',
      ],
      '#limit_validation_errors' => [
        array_merge($form['#parents'] ?? [], ['property']),
      ],
    ];

    $form['property']['#ajax'] = $form['update_property']['#ajax'] + [
      'trigger_as' => [
        'name' => $actions_base_name . '__update_property',
      ],
    ];

    $component_state = &static::getComponentState($form, $form_state);
    if (!empty($component_state['setup_needs_bundle_selection'])) {
      $form['target_bundle'] = [
        '#type' => 'select',
        '#title' => t('Bundle'),
        '#options' => array_map(fn($info) => $info['label'], entity_get_info($component_state['entity_type'])['bundles']),
        '#default_value' => $this->definition['target_bundle'],
      ];
    }

    return $form;
  }

  /**
   * Update the property submit callback.
   */
  public function setupFormSubmitUpdateProperty($form, &$form_state) {
    $button = $form_state['triggering_element'];
    $values = drupal_array_get_nested_value($form_state['values'], array_slice($button['#parents'], 0, -1));

    /** @var \EntityStructureWrapper $structure */
    $structure = $this->templateConfig->getStructure();
    $component_state = &static::getComponentState($button, $form_state);
    if (
      !empty($values['property']) &&
      ($property_info = $structure->getPropertyInfo($values['property'])) &&
      ($entity_info = entity_get_info($property_info['type'])) &&
      !empty($entity_info['entity keys']['bundle'])
    ) {
      $component_state['setup_needs_bundle_selection'] = TRUE;
      $component_state['entity_type'] = $property_info['type'];
      $form_state['rebuild'] = TRUE;
    }
    else if (!empty($component_state['setup_needs_bundle_selection'])) {
      unset($component_state['setup_needs_bundle_selection']);
      unset($component_state['entity_type']);
      $form_state['rebuild'] = TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setupFormSubmit($form, &$form_state) {
    $this->traitSetupFormSubmit($form, $form_state);

    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    if ($values['target_bundle']) {
      $this->definition['target_bundle'] = $values['target_bundle'];
    }

    // Empty the component form.
    $cs = &static::getComponentState($form, $form_state);
    unset($cs['setup_needs_bundle_selection']);
    unset($cs['entity_type']);
  }

  /**
   * Submit callback to refresh the setup form.
   *
   * @param $form
   * @param $form_state
   *
   * @return void
   */
  public static function setupFormSubmitRefreshAjaxCallback($form, &$form_state) {
    $button = $form_state['triggering_element'];
    return drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -1));
  }

  /**
   * {@inheritdoc}
   */
  public function configForm($form, &$form_state) {
    $form = parent::configForm($form, $form_state);

    $form = $this->configurationForm($form, $form_state);
    $form['template']['add_component']['#component'] = static::getComponentState($form, $form_state)['template_config']->getId();
    if (!empty($form['modal_elements'])) {
      foreach (element_children($form['modal_elements']) as $modal_child_name) {
        if ($form['modal_elements'][$modal_child_name]['#type'] === 'submit') {
          $form['modal_elements'][$modal_child_name]['#component'] = $form['template']['add_component']['#component'];
        }
      }
    }

    foreach (element_children($form['template']['components']) as $component_name) {
      if (empty($form['template']['components'][$component_name]['actions'])) {
        continue;
      }

      foreach (element_children($form['template']['components'][$component_name]['actions']) as $component_action) {
        $form['template']['components'][$component_name]['actions'][$component_action]['#component'] = $form['template']['add_component']['#component'];
      }
    }

    return $form;
  }

  public function configFormValidate($form, &$form_state) {
    $this->configurationFormValidate($form, $form_state);
  }

  public function configFormSubmit($form, &$form_state) {
    $this->configurationFormSubmit($form, $form_state);

    $this->configuration['components'] = [];
    foreach ($this->ensureRelevantTemplateConfig($form, $form_state)->getComponents() as $id => $component) {
      $this->configuration['components'][$id] = [
        'plugin' => $component->getPluginId(),
        'definition' => array_diff_key($component->getDefinition(), ['label' => TRUE, 'class' => TRUE]),
        'configuration' => $component->getConfiguration(),
        'priority' => $component->getPriority(),
        'conditions' => $component->getConditions(),
      ];
    }

    parent::configFormSubmit($form, $form_state);
  }

  /**
   * @inheritDoc
   */
  protected function inputMethodLabel(): string {
    return t('Template');
  }

  protected function ensureRelevantTemplateConfig(array $form, array &$form_state): TemplateConfiguration {
    $root_element = static::getComponentForm($form_state['complete form'], $form);

    $property_info = $this->templateConfig->getStructure()->getPropertyInfo($this->definition['property']);

    $component_state = &static::getComponentState($root_element, $form_state);
    $component_state['template_config'] = $component_state['template_config'] ?? (
      entity_get_info($property_info['type']) ?
        new EntityTemplateConfiguration(
          end($root_element['#parents']),
          $property_info['type'],
          $this->definition['target_bundle'] ?? $property_info['type'],
          $this->configuration['components'] ?? [],
          [],
          $this->templateConfig
        ) :
        new StructureTemplateConfiguration(
          end($root_element['#parents']),
          $property_info,
          $this->configuration['components'] ?? [],
          [],
          $this->templateConfig,
        )
    );
    $component_state['template_config']->setFormStateAddress(array_merge(
      ['component_state'],
      $root_element['#parents'] ?? [],
      ['#state', 'template_config']
    ));

    return $component_state['template_config'];
  }

  protected static function &getConfigurationFormState(array $form, array &$form_state): array {
    $cs = &static::getComponentState($form, $form_state);
    return $cs;
  }

  protected static function getRootFormElement(array $element, array $form): array {
    return static::getComponentForm($form, $element);
  }

}
