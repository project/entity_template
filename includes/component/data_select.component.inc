<?php

/**
 * Class for selecting a value from the parameters provided.
 */
class DataSelectEntityTemplateComponent extends EntityTemplateComponentBase implements EntityTemplateComponentRequiringSetupInterface, PropertySettingEntityComponentInterface, EntityTemplateSwappableComponentInterface {
  use PropertySettingEntityTemplateComponentTrait;

  /**
   * @inheritDoc
   */
  public function apply(EntityMetadataWrapper $structure, array $parameters = []) {
    try {
      $wrapper = $structure->get($this->definition['property']);
    }
    catch (\EntityMetadataWrapperExecption $e) {
      drupal_set_message(
        t(
          'Trying to set unknown property @property on type @type; skipping component.',
          ['@property' => $this->definition['property'], '@type' => $structure->type()]
        ),
        'warning'
      );
      return;
    }
    $selector = $this->configuration['select'];

    $processors = $this->configuration['processors'];
    RulesDataProcessor::prepareSetting(
      $processors,
      $wrapper->info(),
      array_map(fn($wrapper) => $wrapper->info(), $parameters),
    );

    // Special behaviour for lists.
    // For lists we allow the use of a comma separated list of properties
    // these properties can be lists in their own right or individual
    // instances of whatever the list contains.
    if ($wrapper instanceof EntityListWrapper) {
      $selectors = explode(',', $selector);
      $values = [];
      foreach ($selectors as $sel) {
        if ($selected = $this->getNestedWrapper($parameters, $sel)) {
          if ($selected instanceof EntityListWrapper) {
            // Selected is a list, so loop over what we have and add them to
            // the property.
            foreach ($selected as $item) {
              try {
                if ($item->value()) {
                  $values[] = $item->value();
                }
              }
              catch (EntityMetadataWrapperException $e) {}
            }
          }
          else {
            // Get the value from the selected wrapper. We wrap this in a try
            // block to make sure that the system doesn't completely fail when
            // one of the parent values is missing.
            try {
              if (is_callable(array($selected, 'value')) && $selected->value()) {
                $values[] = $selected->value();
              }
            }
            catch (EntityMetadataWrapperException $e) {}
          }
        }
      }

      if (!empty($processors)) {
        $state = new RulesState();
        foreach ($parameters as $name => $param_wrapper) {
          if ($name == 'site') {
            continue;
          }
          $state->addVariable($name, $param_wrapper->value(), $param_wrapper->info());
        }

        $values = $processors->process($values, $wrapper->info(), $state, new RulesAction('entity_template_create'));
      }

      $values = $this->applyModifiers($values, $parameters);
      if (empty($this->configuration['list_behavior']) || $this->configuration['list_behavior'] === 'replace') {
        $wrapper->set($values);
      }
      else if ($this->configuration['list_behavior'] === 'append') {
        $wrapper->set(array_merge($wrapper->value(), $values));
      }
      else if ($this->configuration['list_behavior'] === 'prepend') {
        $wrapper->set(array_merge($values, $wrapper->value()));
      }
    }
    else {
      $selected = $this->getNestedWrapper($parameters, $selector);
      try {
        $value = is_object($selected) ? $selected->value() : NULL;
        if (!empty($processors)) {
          $state = new RulesState();
          foreach ($parameters as $name => $param_wrapper) {
            if ($name == 'site') {
              continue;
            }
            $state->addVariable($name, $param_wrapper->value(), $param_wrapper->info());
          }

          $value = $processors->process($value, $wrapper->info(), $state, new RulesAction('entity_template_create'));
        }

        $value = $this->applyModifiers($value, $parameters);
        if ($wrapper->type() === 'text' && !empty($this->configuration['text_behavior'])) {
          switch ($this->configuration['text_behavior']) {
            case 'prepend':
              $wrapper->set($value . $wrapper->value());
              break;
            case 'append':
              $wrapper->set($wrapper->value() . $value);
              break;
            case 'replace':
            default:
              $wrapper->set($value);
              break;
          }
        }
        else if ($wrapper->type() === 'text_formatted' && !empty($this->configuration['text_behavior'])) {
          switch ($this->configuration['text_behavior']) {
            case 'prepend':
              $wrapper->set([
                'value' => $value['value'] . $wrapper->value->value(),
                'format' => $wrapper->format->value() ?: $value['format'],
              ]);
              break;
            case 'append':
              $wrapper->set([
                'value' => $wrapper->value->value() . $value['value'],
                'format' => $wrapper->format->value() ?: $value['format'],
              ]);
              break;
            case 'replace':
            default:
              $wrapper->set($value);
              break;
          }
        }
        else {
          $wrapper->set($value);
        }
      }
      catch (EntityMetadataWrapperException $e) {}
    }
  }

  /**
   * {@inheritdoc}
   */
  public function configForm($form, &$form_state) {
    /** @var \EntityDrupalWrapper $structure */
    $structure = $this->templateConfig->getStructure();

    $target_property_info = $structure->getPropertyInfo($this->definition['property']);
    if (entity_property_list_extract_type($target_property_info['type'])) {
      $form['list_behavior'] = [
        '#type' => 'select',
        '#title' => t('List Behavior'),
        '#options' => [
          'prepend' => t('Add to the start of the list.'),
          'replace' => t('Set the list.'),
          'append' => t('Add to the end of the list.'),
        ],
        '#default_value' => $this->configuration['list_behavior'] ?? 'replace',
      ];
    }

    if (in_array($target_property_info['type'], ['text', 'text_formatted']) && empty($target_property_info['options list'])) {
      $form['text_behavior'] = [
        '#type' => 'select',
        '#title' => t('Text Behavior'),
        '#options' => [
          'prepend' => t('Add to the start of the text.'),
          'replace' => t('Set the text.'),
          'append' => t('Add to the end of the text.'),
        ],
        '#default_value' => $this->configuration['text_behavior'] ?? 'replace',
      ];
    }

    form_load_include($form_state, 'inc', 'rules', 'ui/ui.forms');

    $tcAddr = $this->templateConfig->getFormStateAddress() ?
      implode(':', $this->templateConfig->getFormStateAddress()) :
      'null';
    if ($structure instanceof EntityDrupalWrapper) {
      $bundle = $structure->getBundle() ?: $structure->info()['bundle'];
      $acp = "entity_template/selector/{$tcAddr}/{$structure->type()}/{$bundle}/{$this->definition['property']}";
    }
    else {
      $key = md5(json_encode($structure->getPropertyInfo()));
      $form_state['entity_template_selector_structs'][$key] = $structure->getPropertyInfo();
      $acp = "entity_template/selector/{$tcAddr}/struct/{$key}/{$this->definition['property']}";
    }
    $form['select'] = [
      '#type' => 'rules_data_selection',
      '#title' => t('Select'),
      '#default_value' => $this->configuration['select'],
      '#autocomplete_path' => $acp,
      '#size' => 75,
    ];

    $form['processors'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#title' => t('Processing Options'),
    ];
    $options_element = &$form['processors'];
    RulesDataProcessor::attachForm(
      $options_element,
      ($this->configuration['processors'] ?? []) ?: [],
      $structure->getPropertyInfo($this->definition['property']),
      $this->templateConfig->getParametersInfo(),
    );
    $options_element['#access'] = count(element_children($options_element));

    return parent::configForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function configFormSubmit($form, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents'] ?? []);
    $this->configuration['list_behavior'] = $values['list_behavior'];
    $this->configuration['text_behavior'] = $values['text_behavior'];
    $this->configuration['select'] = $values['select'];
    $this->configuration['processors'] = $values['processors'];

    parent::configFormSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function inputMethodLabel(): string {
    return t('Select');
  }

  /**
   * Get a nested property.
   */
  protected function getNestedWrapper($wrapped_params, $selector) {
    $parts = explode(':', $selector);
    $namespace = str_replace('-', '_', array_shift($parts));
    $selected = $wrapped_params[$namespace];

    if (empty($selected)) {
      return FALSE;
    }

    try {
      while (($sub = array_shift($parts)) !== NULL) {
        try {
          $sub = str_replace('-', '_', $sub);
          $selected = $selected->get($sub);
        }
        catch (EntityMetadataWrapperException $e) {
          return FALSE;
        }
      }
    }
    catch (Exception $e) {
      return FALSE;
    }

    return $selected;
  }

}
