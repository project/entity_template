<?php

class InputValueEntityTemplateComponent__Property extends InputValueEntityTemplateComponent {

  /**
   * {@inheritdoc}
   */
  public function configForm($form, &$form_state) {
    /** @var \EntityStructureWrapper $structure */
    $structure = $this->templateConfig->getStructure();
    $property_info = $structure->getPropertyInfo($this->definition['property']);
    $property = $structure->get($this->definition['property']);

    $element = array(
      '#title' => $property_info['label'],
      '#description' => $property_info['description'],
      '#default_value' => $this->configuration['value'],
    );

    if (isset($property_info['options list'])) {
      $element['#type'] = $property instanceof EntityListWrapper ?
        'checkboxes' :
        'select';
      $element['#options'] = $property->optionsList();
    }
    else if ($property_info['type'] == 'boolean') {
      $element['#type'] = 'checkbox';
    }
    else if ($entity_info = entity_get_info($property_info['type'])) {
      // an auto complete.
    }
    else {
      $element['#type'] = 'textfield';
    }

    $form['value'] = $element;

    return parent::configForm($form, $form_state);
  }

  public function configFormSubmit($form, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['#parents']);
    $this->configuration['value'] = $values['value'];

    parent::configFormSubmit($form, $form_state);
  }

}
