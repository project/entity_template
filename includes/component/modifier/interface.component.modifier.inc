<?php

/**
 * Interface for a component modifier.
 */
interface EntityTemplateComponentModifierInterface {

  /**
   * Get the plugin definition.
   *
   * @return array
   */
  public function getDefinition() : array;

  /**
   * Modify the value.
   *
   * @param mixed $value
   *   The value provided to modify.
   *
   * @return mixed
   *   Should be of the same type as provided.
   */
  public function modify($value);

  /**
   * Check whether the modifier can modify the proposed value.
   *
   * @param \EntityTemplateComponentInterface $component
   *   The component.
   *
   * @return bool
   */
  public function canModify(EntityTemplateComponentInterface $component) : bool;

  /**
   * Set the component the modifier is acting on.
   *
   * @param \EntityTemplateComponentInterface $component
   *   The component.
   *
   * @return $this
   */
  public function setComponent(EntityTemplateComponentInterface $component) : EntityTemplateComponentModifierInterface;

}

/**
 * Interface for a modifier that has configuration.
 */
interface ConfigurableEntityTemplateComponentModifierInterface extends EntityTemplateComponentModifierInterface {

  /**
   * Get the configuration.
   *
   * @return array
   *   The configuration.
   */
  public function getConfiguration() : array;

  /**
   * Set the configuration.
   *
   * @param array $configuration
   *   The configuration.
   *
   * @return $this
   */
  public function setConfiguration(array $configuration) : ConfigurableEntityTemplateComponentModifierInterface;

  /**
   * Build a configuration form.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return array
   */
  public function configurationForm($form, &$form_state) : array;

  /**
   * Validate the configuration form.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return void
   */
  public function configurationFormValidate($form, &$form_state);

  /**
   * Submit the configuration form.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return void
   */
  public function configurationFormSubmit($form, &$form_state);
}

/**
 * Interface for a modifier that needs to know about parameters.
 */
interface ParameterAwareEntityTemplateComponentModifierInterface extends EntityTemplateComponentModifierInterface {

  /**
   * Set the parameters.
   *
   * @param array $parameters
   *   The parameters.
   *
   * @return $this
   */
  public function setParameters(array $parameters) : ParameterAwareEntityTemplateComponentModifierInterface;

}
