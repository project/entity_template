<?php

/**
 * Trait for parameter aware component modifiers.
 */
trait ParameterAwareEntityTemplateComponentModifierTrait {

  /**
   * The parameters.
   * @var array
   */
  protected $parameters = [];

  /**
   * The parameter structure.
   *
   * @var \EntityStructureWrapper|NULL
   */
  protected $parameterStructure = NULL;

  /**
   * Set the parameters.
   *
   * @param array $parameters
   *    The parameters.
   *
   * @return $this
   *
   * @see \ParameterAwareEntityTemplateComponentModifierInterface::setParameters()
   */
  public function setParameters(array $parameters) : \ParameterAwareEntityTemplateComponentModifierInterface {
    $this->parameters = $parameters;
    return $this;
  }

  /**
   * Get a structure representing the parameters.
   *
   * @return \EntityStructureWrapper
   */
  protected function getParameterStructure() : \EntityStructureWrapper {
    if (is_null($this->parameterStructure)) {
      $this->parameterStructure = entity_metadata_wrapper(
        'parameters',
        array_map(
          function ($p) {
            try {
              return $p->raw();
            }
            catch (EntityMetadataWrapperException $exception) {
              return NULL;
            }
          },
          $this->parameters
        ),
        [
          'type' => 'parameters',
          'property info' => array_map(fn($p) => $p->info(), $this->parameters),
        ]
      );
    }

    return $this->parameterStructure;
  }

}

/**
 * Trait for configurable component modifiers.
 */
trait ConfigurableEntityTemplateComponentModifierTrait {

  /**
   * The configuration.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * Get the configuration.
   *
   * @return array
   *    The configuration.
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * Set the configuration.
   *
   * @param array $configuration
   *    The configuration.
   *
   * @return $this
   */
  public function setConfiguration(array $configuration): ConfigurableEntityTemplateComponentModifierInterface {
    $this->configuration = $configuration;
    return $this;
  }

  /**
   * Validate the configuration form.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return void
   */
  public function configurationFormValidate($form, &$form_state) {}

}
