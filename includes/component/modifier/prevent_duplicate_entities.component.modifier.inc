<?php

/**
 * Prevent duplicate entities on sub templates.
 */
class PreventDuplicateEntitiesComponentModifier extends EntityTemplateComponentModifierBase implements ConfigurableEntityTemplateComponentModifierInterface {
  use ConfigurableEntityTemplateComponentModifierTrait;

  /**
   * The component.
   *
   * @var \NestedStructureTemplateEntityTemplateComponent
   */
  protected EntityTemplateComponentInterface $component;

  /**
   * {@inheritdoc}
   */
  public function modify($value) {
    $query = new EntityFieldQuery(); $execute = FALSE;
    $target_definition = $this->component->getTargetPropertyDefinition();
    $wrapper = entity_metadata_wrapper($target_definition['type'], $value, $target_definition);
    $query->entityCondition('entity_type', $target_definition['type']);
    if ($wrapper->getBundle()) {
      $query->entityCondition('bundle', $wrapper->getBundle());
    }
    foreach ($this->configuration['properties'] as $property) {
      [$property, $sub_property] = explode(':', $property);
      $property_info = $wrapper->getPropertyInfo($property);

      if (!empty($property_info['field'])) {
        if ($wrapper->get($property) instanceof EntityStructureWrapper) {
          $execute = TRUE;
          try {
            $search_value = $wrapper->get($property)->get($sub_property);
          }
          catch (\EntityMetadataWrapperException $e) {
            $search_value = NULL;
          }

          $query->fieldCondition(
            $property,
            $sub_property,
            $search_value
          );
        }
        else {
          $execute = TRUE;
          $field = field_info_field($property);
          $query->fieldCondition($property, key($field['columns']), $wrapper->get($property)->raw());
        }
      }
      else if (!empty($property_info['schema field'])) {
        $execute = TRUE;
        $query->propertyCondition($property_info['schema field'], $wrapper->get($property)->raw());
      }
    }

    if ($execute && ($result = $query->execute()) && !empty($result[$target_definition['type']])) {
      $value = key($result[$target_definition['type']]);
    }

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function canModify(EntityTemplateComponentInterface $component): bool {
    if (
      $component instanceof PropertySettingEntityComponentInterface &&
      entity_get_info($component->getTargetPropertyDefinition()['type']) &&
      $component instanceof NestedStructureTemplateEntityTemplateComponent
    ) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm($form, &$form_state): array {
    $template = new EntityTemplateConfiguration(
      $this->component->suggestConfigKey(),
      $this->component->getTargetPropertyDefinition()['type'],
      $this->component->getDefinition()['target_bundle'] ?? $this->component->getTargetPropertyDefinition()['type'],
      $this->component->getConfiguration()['components'] ?? [],
      []
    );

    $property_options = [];
    foreach ($template->getComponents() as $component) {
      if (!($component->getPlugin() instanceof PropertySettingEntityComponentInterface)) {
        continue;
      }

      $property_info = $component->getPlugin()->getTargetPropertyDefinition();
      if ($property_info['schema field']) {
        $property_options[$component->getDefinition()['property']] = $property_info['label'];
      }
      else if ($property_info['field'] === TRUE) {
        if (!empty($property_info['property info'])) {
          foreach ($property_info['property info'] as $sub_property => $sub_property_info) {
            $property_options[$component->getPlugin()->getDefinition()['property'] . ':' . $sub_property] = $property_info['label'] . ' > ' . $sub_property_info['label'];
          }
        }
        else {
          $property_options[$component->getPlugin()->getDefinition()['property']] = $property_info['label'];
        }
      }
    }

    $form['properties'] = [
      '#type' => 'checkboxes',
      '#title' => t('Properties'),
      '#options' => $property_options,
      '#default_value' => $this->configuration['properties'],
      '#required' => TRUE,
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function configurationFormSubmit($form, &$form_state) {
    $values = drupal_array_get_nested_value($form_state['values'], $form['properties']['#parents']);
    $this->configuration['properties'] = array_filter($values);
  }

}
