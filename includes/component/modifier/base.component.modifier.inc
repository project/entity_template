<?php

abstract class EntityTemplateComponentModifierBase implements EntityTemplateComponentModifierInterface {

  /**
   * The plugin id.
   *
   * @var string
   */
  protected string $pluginId;

  /**
   * The plugin definition.
   *
   * @var array
   */
  protected array $pluginDefinition;

  /**
   * The component.
   *
   * @var \EntityTemplateComponentInterface
   */
  protected EntityTemplateComponentInterface $component;

  /**
   * Construct a new object.
   *
   * @param string $plugin_id
   *   Plugin id.
   * @param array $definition
   *   The plugin definition.
   */
  public function __construct(string $plugin_id, array $definition) {
    $this->pluginId = $plugin_id;
    $this->pluginDefinition = $definition;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition() : array {
    return $this->pluginDefinition;
  }

  /**
   * {@inheritdoc}
   */
  public function setComponent(EntityTemplateComponentInterface $component): EntityTemplateComponentModifierInterface {
    $this->component = $component;
    return $this;
  }

}
