<?php

/**
 *
 */
class CtoolsTokensComponentModifier extends EntityTemplateComponentModifierBase implements ParameterAwareEntityTemplateComponentModifierInterface {
  use ParameterAwareEntityTemplateComponentModifierTrait;

  /**
   * {@inheritdoc}
   */
  public function modify($value) {
    $contexts = $this->getCtoolsContexts($this->parameters);

    if (is_string($value)) {
      return $this->applyReplacements($value, $contexts);
    }
    else if (is_array($value)) {
      if (is_numeric(key($value))) {
        foreach ($value as $delta => $item_value) {
          if (is_string($item_value)) {
            $value[$delta] = $this->applyReplacements($item_value, $contexts);
          }
          else if (!empty($item_value['value'])) {
            $value[$delta]['value'] = $this->applyReplacements($item_value['value'], $contexts);
          }
        }
      }
      else if (isset($value['value']) && is_string($value['value'])) {
        $value['value'] = $this->applyReplacements($value['value'], $contexts);
      }
    }

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function canModify(EntityTemplateComponentInterface $component): bool {
    if ($component instanceof PropertySettingEntityComponentInterface) {
      $property_info = $component->getTargetPropertyDefinition();
      if ($property_info['type'] === 'text' && empty($property_info['options list'])) {
        return TRUE;
      }
      if ($property_info['type'] === 'list<text>' && empty($property_info['options list'])) {
        return TRUE;
      }
      if ($property_info['type'] === 'text_formatted') {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Apply the replacements.
   *
   * @param string $string
   *   The string.
   *
   * @return string
   *   String with replacements applied.
   */
  protected function applyReplacements(string $string, array $contexts) : string {
    $string = str_replace('%', '@|', $string);
    $string = str_replace('@:', '%', $string);
    $string = ctools_context_keyword_substitute($string, [], $contexts, ['sanitize' => FALSE]);
    $string = str_replace('@|', '%', $string);

    return $string;
  }

  /**
   * Get an array of ctools contexts for the replacements.
   *
   * @param array $parameters
   *   An array of entities keyed by namespace. If NULL, placeholder contexts
   *   will be used.
   *
   * @return ctools_context[]
   *   An array of ctools contexts.
   */
  protected function getCtoolsContexts($parameters = array()) {
    global $user;

    ctools_include('context');
    $contexts = array(
      'site' => ctools_context_create('token'),
      'current-user' => ctools_context_create('entity:user', $user),
    );
    $contexts['site']->keyword = 'global';
    $contexts['current-user']->keyword = 'current-user';
    $contexts['current-user']->identifier = t('Logged-in user');

    $available_context_plugins = ctools_get_contexts();
    if (is_array($parameters)) {
      foreach ($parameters as $namespace => $wrapper) {
        if ($wrapper instanceof EntityDrupalWrapper) {
          $type = 'entity:' . $wrapper->type();
        }
        else if ($wrapper->type() == 'text') {
          $type = 'string';
        }
        else if (!empty($available_context_plugins[$wrapper->type()])) {
          $type = $wrapper->type();
        }
        else {
          continue;
        }

        if ($wrapper->value()) {
          $contexts[$namespace] = ctools_context_create($type, $wrapper->value());
        }
        else {
          $contexts[$namespace] = ctools_context_create_empty($type);
        }
        $contexts[$namespace]->keyword = $namespace;
        $contexts[$namespace]->identifier = $wrapper->label();
      }
    }

    return $contexts;
  }

}
