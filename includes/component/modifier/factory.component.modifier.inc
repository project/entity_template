<?php

/**
 * Factory for component modifiers.
 */
class EntityTemplateComponentModifierFactory {

  /**
   * Get the plugin definitions.
   *
   * @return array
   *   The definitions keyed by name.
   */
  public static function getDefinitions() {
    $plugins = &drupal_static(__FUNCTION__, array());
    if (!empty($plugins)) {
      return $plugins;
    }

    if ($cached = cache_get(__METHOD__)) {
      $plugins = $cached->data;
    }
    else {
      $plugins = module_invoke_all('entity_template_component_modifier_plugin_info');
      drupal_alter('entity_template_component_modifier_plugin_info', $plugins);

      cache_set(__METHOD__, $plugins);
    }

    return $plugins;
  }

  /**
   * Get a plugin definition.
   *
   * @param string $plugin_id
   *   The plugin id.
   *
   * @return array
   *   A plugin definition.
   */
  public static function getDefinition($plugin_id) {
    return static::getDefinitions()[$plugin_id] ?? NULL;
  }

  /**
   * Create an instance of the plugin.
   *
   * @param string $plugin_id
   *   The plugin id.
   * @param array $configuration
   *   The configuration.
   *
   * @return \EntityTemplateComponentModifierInterface
   */
  public static function createInstance($plugin_id, array $configuration = []) {
    $definition = static::getDefinition($plugin_id);

    $instance = new $definition['class']($plugin_id, $definition);
    if ($instance instanceof ConfigurableEntityTemplateComponentModifierInterface) {
      $instance->setConfiguration($configuration);
    }

    return $instance;
  }

  /**
   * Check whether a given component has any modifiers that apply.
   *
   * @param \EntityTemplateComponentInterface $component
   *   The component to check.
   *
   * @return bool
   */
  public static function componentHasModifiers(EntityTemplateComponentInterface $component) : bool {
    foreach (static::getDefinitions() as $id => $definition) {
      if (static::createInstance($id)->canModify($component)) {
        return TRUE;
      }
    }

    return FALSE;
  }
}
