<?php

/**
 * Special class for inputting the field value directly
 */
class InputValueEntityTemplateComponent__Field extends InputValueEntityTemplateComponent {

  /**
   * In this plugin the template config is always an entity template.
   *
   * @var \EntityTemplateConfiguration
   */
  protected TemplateConfiguration $templateConfig;

  /**
   * {@inheritdoc}
   */
  public function configForm($form, &$form_state) {
    $items = $this->configuration['value'];

    // field_default_form expects $items to be an array or NULL but
    // field_get_items returns FALSE when there are none. If $items is FALSE
    // turn it into an empty array to prevent errors later on.
    if (!$items) {
      $items = array();
    }

    $field = field_info_field($this->definition['property']);
    $instance = field_info_instance($this->templateConfig->getEntityType(), $this->definition['property'], $this->templateConfig->getBundle());
    if (!$field || !$instance) {
      debug("Error in " . __CLASS__ . "::" . __METHOD__ .": Field <em>{$this->definition['property']}</em> does not exist or does not have an instance on {$this->templateConfig->getEntityType()}:{$this->templateConfig->getBundle()}.", "error");
      return $form;
    }

    $entity = $this->templateConfig->getStructure()->value();
    $entity->{$this->definition['property']}[LANGUAGE_NONE] = $items;
    $form_fields = field_default_form(
      $this->templateConfig->getEntityType(),
      $entity,
      $field,
      $instance,
      LANGUAGE_NONE,
      $items,
      $form,
      $form_state
    );
    foreach ($form_fields as $name => $element) {
      $element['#parents'] = $form['#parents'];
      $element['#parents'][] = $this->definition['property'];

      $form[$this->definition['property']] = $element;
    }

    return parent::configForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function configFormValidate($form, &$form_state) {
    parent::configFormValidate($form, $form_state);

    $entity = $this->templateConfig->getStructure()->value();

    _field_invoke_default('extract_form_values', $this->templateConfig->getEntityType(), $entity, $form, $form_state);

    try {
      $errors = array();
      $null = NULL;

      _field_invoke_default('validate', $this->templateConfig->getEntityType(), $entity, $errors, $null);
      _field_invoke('validate', $this->templateConfig->getEntityType(), $entity, $errors, $null);

      // Let other modules validate the entity.
      foreach (module_implements('field_attach_validate') as $module) {
        $function = $module . '_field_attach_validate';
        $function($this->templateConfig->getEntityType(), $entity, $errors);
      }

      if ($errors) {
        throw new FieldValidationException($errors);
      }
    }
    catch (FieldValidationException $exception) {
      // Pass field-level validation errors back to widgets for accurate error
      // flagging.
      foreach ($exception->errors as $field_name => $field_errors) {
        foreach ($field_errors as $langcode => $errors) {
          $field_state = field_form_get_state($form['#parents'], $field_name, $langcode, $form_state);
          $field_state['errors'] = $errors;
          field_form_set_state($form['#parents'], $field_name, $langcode, $form_state, $field_state);
        }
      }

      _field_invoke_default('form_errors', $entity, $form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function configFormSubmit($form, &$form_state) {
    $entity = $this->templateConfig->getStructure()->value();
    field_attach_submit(
      $this->templateConfig->getEntityType(),
      $entity,
      $form,
      $form_state,
      array('field_name' => $this->definition['property'])
    );

    $this->configuration['value'] = $entity->{$this->definition['property']}[LANGUAGE_NONE];

    parent::configFormSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function apply(EntityMetadataWrapper $structure, array $parameters = array()) {
    $entity = $structure->value();
    $items = $this->applyModifiers($this->configuration['value'], $parameters);

    if (
      entity_property_list_extract_type($structure->{$this->definition['property']}->type()) &&
      !empty($this->configuration['list_behavior'])
    ) {
      switch ($this->configuration['list_behavior']) {
        case 'prepend':
          $items = array_merge($items ?? [], $entity->{$this->definition['property']}[LANGUAGE_NONE] ?? []);
          if (in_array(field_info_field($this->definition['property'])['type'], ['list_text', 'list_integer', 'list_float', 'entityreference'])) {
            $items = array_values(array_unique($items ?? [], SORT_REGULAR));
          }
          break;
        case 'append':
          $items = array_merge($entity->{$this->definition['property']}[LANGUAGE_NONE] ?? [], $items ?? []);
          if (in_array(field_info_field($this->definition['property'])['type'], ['list_text', 'list_integer', 'list_float', 'entityreference'])) {
            $items = array_values(array_unique($items ?? [], SORT_REGULAR));
          }
          break;
        case 'replace':
        default:
          break;
      }
    }
    else if (
      in_array($structure->{$this->definition['property']}->type(), ['text', 'text_formatted']) &&
      !empty($this->configuration['text_behavior'])
    ) {
      foreach ($items as $delta => $item) {
        switch ($this->configuration['text_behavior']) {
          case 'prepend':
            $items[$delta]['value'] = $items[$delta]['value'] . ($entity->{$this->definition['property']}[LANGUAGE_NONE][$delta]['value'] ?? '');
            $items[$delta]['format'] = $entity->{$this->definition['property']}[LANGUAGE_NONE][$delta]['value'] ?? $items[$delta]['format'];
            break;
          case 'append':
            $items[$delta]['value'] = ($entity->{$this->definition['property']}[LANGUAGE_NONE][$delta]['value'] ?? '') . $items[$delta]['value'];
            $items[$delta]['format'] = $entity->{$this->definition['property']}[LANGUAGE_NONE][$delta]['value'] ?? $items[$delta]['format'];
            break;
          case 'replace':
          default:
            break;
        }
      }
    }

    $entity->{$this->definition['property']}[LANGUAGE_NONE] = $items;
  }

}
