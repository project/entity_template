<?php

interface EntityTemplateSwappableComponentInterface extends EntityTemplateComponentInterface {

  /**
   * Check whether this can swapped for the given component.
   *
   * @param \EntityTemplateComponentInterface $component
   *   The component to be swapped out.
   *
   * @return bool
   */
  public function canSwapFor(EntityTemplateComponentInterface $component) : bool;

  /**
   * Prepare the definition of the swap component based on the original.
   *
   * @param \EntityTemplateComponentInterface $component
   *   The component being swapped out.
   *
   * @return array
   *   The definition of a new component.
   */
  public function prepareSwapDefinitionFor(EntityTemplateComponentInterface $component) : array;

  /**
   * Prepare the swap configuration for this component.
   *
   * @param \EntityTemplateComponentInterface $component
   *   The component being swapped.
   *
   * @return array
   */
  public function prepareSwapConfigurationFor(EntityTemplateComponentInterface $component) : array;

}
