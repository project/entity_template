<?php

use Drupal\ck_opencrm\Plugin\ConditionSetEvaluator\ConditionSetEvaluatorManager;
use Drupal\Component\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;

/**
 * Template configuration.
 */
abstract class TemplateConfiguration {

  /**
   * The id of this template configuration.
   *
   * @var string
   */
  protected string $id;

  /**
   * The template components.
   *
   * @var \TemplateComponentConfiguration[]
   */
  protected array $components = array();

  /**
   * The parameters associated with this template.
   *
   * @var array
   */
  protected array $parameters = [];

  /**
   * The ids of any parent templates.
   *
   * @var \TemplateConfiguration
   */
  protected ?TemplateConfiguration $parent = NULL;

  /**
   * The form state address.
   *
   * This variable allows the configuration to tell components how it can be
   * addressed in the form state.
   *
   * @var array
   */
  protected array $formStateAddress;

  /**
   * Get the structure.
   *
   * @return \EntityMetadataWrapper
   *   An entity metadata wrapper object describing the structure to be created.
   */
  public abstract function getStructure() : EntityMetadataWrapper;

  /**
   * Get information about the parameters keyed by name.
   *
   * @return array
   *   The parameter info.
   */
  public function getParametersInfo(): array {
    if ($this->parent) {
      return $this->parameters + $this->parent->getParametersInfo();
    }

    return $this->parameters;
  }

  /**
   * Get the id.
   *
   * @return string
   */
  public function getId(): string {
    return ($this->parent ? $this->parent->getId() . '.' : '') . $this->id;
  }

  /**
   * Initialize the components supplied.
   *
   * @param array $components
   *
   * @return void
   */
  protected function initComponents(array $components) {
    foreach ($components as $name => $component) {
      $this->components[$name] = $component instanceof TemplateComponentConfiguration ?
        $component :
        new TemplateComponentConfiguration(
          $component['plugin'],
          $component['definition'] ?? array(),
          $component['configuration'] ?? array(),
          $component['priority'] ?? 0,
          $component['conditions'] ?? array()
        );
      $this->components[$name]->setTemplateConfiguration($this);
    }
  }

  /**
   * Get a specific component.
   *
   * @param string $name
   *   The name.
   *
   * @return \TemplateComponentConfiguration
   */
  public function getComponent(string $name) : TemplateComponentConfiguration {
    return $this->components[$name];
  }

  /**
   * Get the template component configurations.
   *
   * @return \TemplateComponentConfiguration[]
   */
  public function getComponents($sorted = FALSE): array {
    if ($sorted) {
      uasort($this->components, function(TemplateComponentConfiguration $comp_a, TemplateComponentConfiguration $comp_b) {
        $a = $comp_a->getPriority();
        $b = $comp_b->getPriority();

        if ($a == $b) {
          return 0;
        }
        return ($a > $b) ? -1 : 1;
      });
    }

    return $this->components;
  }

  /**
   * Add a component to this template configuration.
   *
   * @param \TemplateComponentConfiguration $component
   *   The component configuration.
   * @param string|NULL $id
   *   The id of the component if known.
   *
   * @return $this
   */
  public function addComponent(TemplateComponentConfiguration $component, string $id = NULL) {
    $id = $id ?? (
      $component->getPlugin()->suggestConfigKey() ?:
      (function_exists('uuid_generate') ? uuid_generate() : uniqid('comp-', TRUE))
    );

    $initid = $id; $d = 1;
    while (isset($this->components[$id])) {
      $id = "{$initid}-{$d}";
      $d++;
    }

    $component->setTemplateConfiguration($this);
    $this->components[$id] = $component;

    return $this;
  }

  /**
   * Replace an already existing component.
   *
   * @param string $id
   *   The id to replace.
   * @param \TemplateComponentConfiguration $component
   *   The component
   *
   * @return $this
   *
   * @throws \Exception
   *   If the component does not exist.
   */
  public function replaceComponent(string $id, TemplateComponentConfiguration $component) {
    if (!isset($this->components[$id])) {
      throw new \Exception('Trying to replace a non-existent template component, use addComponent instead.');
    }

    $component->setTemplateConfiguration($this);
    $this->components[$id] = $component;
    return $this;
  }

  /**
   * Remove a component from the template configuration.
   *
   * @param string $id
   *   The id to remove.
   *
   * @return $this
   *
   * @throws \Exception
   *   If the component does not exist.
   */
  public function removeComponent(string $id) : TemplateConfiguration {
    if (!isset($this->components[$id])) {
      throw new \Exception('Trying to remove a non-existent template component..');
    }

    unset($this->components[$id]);
    return $this;
  }

  /**
   * Set the components.
   *
   * @param \TemplateComponentConfiguration[] $components
   *   The array of components keyed by id.
   *
   * @return $this
   */
  public function setComponents(array $components): TemplateConfiguration {
    $this->components = $components;

    foreach ($this->components as $component) {
      $component->setTemplateConfiguration($this);
    }

    return $this;
  }

  /**
   * Set the form state address.
   *
   * @param array $address
   *
   * @return $this
   */
  public function setFormStateAddress(array $address) : TemplateConfiguration {
    $this->formStateAddress = $address;
    return $this;
  }

  /**
   * Get the form state address.
   *
   * @return array|null
   */
  public function getFormStateAddress() : ?array {
    return $this->formStateAddress ?? NULL;
  }

}

/**
 * Template configuration for a list of things.
 */
class ListTemplateConfiguration extends TemplateConfiguration {

  /**
   * List info.
   *
   * @var array
   */
  protected array $listInfo;

  /**
   * The item type.
   *
   * @var string
   */
  protected string $itemType;

  /**
   * The item info.
   *
   * @var string
   */
  protected array $itemInfo;

  /**
   * Construct a new list template configuration.
   *
   * @param string $id
   *   The id of this template.
   * @param array $list_info
   *   The definition of the structure.
   * @param array $components
   *   The components configuration.
   * @param \TemplateConfiguration|null $parent
   *   The parent template if there is one.
   */
  public function __construct(string $id, array $list_info, array $components = array(), TemplateConfiguration $parent = NULL) {
    $this->id = $id;
    $this->listInfo = $list_info;
    $this->itemType = entity_property_list_extract_type($list_info['type']);
    $this->itemInfo = array(
      'type' => $this->itemType,
    ) + $list_info;
    $this->parent = $parent;

    $this->initComponents($components);
  }

  /**
   * {@inheritdoc}
   */
  public function getStructure(): EntityListWrapper {
    return entity_metadata_wrapper($this->listInfo['type'], NULL, $this->listInfo);
  }

  /**
   * {@inheritdoc}
   */
  public function getParametersInfo(): array {
    if ($this->parent) {
      return [] + $this->parent->getParametersInfo();
    }

    return [];
  }

}

class StructureTemplateConfiguration extends TemplateConfiguration {

  /**
   * The structure definition.
   *
   * @var array
   */
  protected array $structureInfo = [];


  /**
   * Construct a new structure template configuration.
   *
   * @param string $id
   *   The id of this template.
   * @param array $structure_info
   *   The definition of the structure.
   * @param array $components
   *   The components configuration.
   * @param array $additional_parameters
   *   Additional parameters not included in the parent.
   * @param \TemplateConfiguration|null $parent
   *   The parent template if the is one.
   */
  public function __construct(string $id, array $structure_info, array $components = [], array $additional_parameters = [], TemplateConfiguration $parent = NULL) {
    $this->id = $id;
    $this->structureInfo = $structure_info;
    $this->parameters = $additional_parameters;
    $this->parent = $parent;

    $this->initComponents($components);
  }

  /**
   * {@inheritdoc}
   */
  public function getStructure(): EntityMetadataWrapper {
    return entity_metadata_wrapper(
      $this->structureInfo['type'],
      NULL,
      $this->structureInfo
    );
  }

}

class EntityTemplateConfiguration extends TemplateConfiguration {

  /**
   * The entity type being generated by this config.
   *
   * @var string
   */
  protected string $entityType;

  /**
   * The bundle being generated by this config.
   *
   * @var string
   */
  protected string $bundle;

  /**
   * Construct a new template config.
   *
   * @param string $id
   *   The id of this template config.
   * @param string $entity_type
   *   The entity type created by this template configurataion.
   * @param string $bundle
   *   The bundle created by this template configuration.
   * @param array $components
   *   The components configuration.
   * @param array $parameters
   *   The parameters information.
   * @param \TemplateConfiguration|null $parent
   *   The parents.
   */
  public function __construct(string $id, string $entity_type, string $bundle, array $components = [], array $parameters = [], TemplateConfiguration $parent = NULL) {
    $this->id = $id;
    $this->entityType = $entity_type;
    $this->parameters = $parameters;
    $this->bundle = $bundle;
    $this->parent = $parent;

    $this->initComponents($components);
  }

  /**
   * {@inheritdoc}
   */
  protected function initComponents(array $components) {
    if (empty($components)) {
      foreach (module_implements('entity_template_default_components') as $module) {
        $function = $module . '_entity_template_default_components';
        $function($components, $this);
      }
    }

    parent::initComponents($components);
  }

  /**
   * Get the entity type.
   *
   * @return string
   */
  public function getEntityType(): string {
    return $this->entityType;
  }

  /**
   * Get the bundle.
   *
   * @return string
   */
  public function getBundle(): string {
    return $this->bundle;
  }


  /**
   * {@inheritdoc}
   */
  public function getStructure(): EntityMetadataWrapper {
    $values = [];
    if (!empty(entity_get_info($this->getEntityType())['entity keys']['bundle'])) {
      $values[entity_get_info($this->getEntityType())['entity keys']['bundle']] = $this->getBundle();
    }

    return entity_metadata_wrapper(
      $this->getEntityType(),
      entity_create($this->getEntityType(), $values),
      array(
        'bundle' => $this->getBundle(),
      )
    );
  }
}

class TemplateComponentConfiguration {

  /**
   * The plugin being used.
   *
   * @var string
   */
  protected string $plugin;

  /**
   * Plugin definition fields.
   *
   * @var array
   */
  protected array $definition = [];

  /**
   * Component configuration info.
   *
   * @var array
   */
  protected array $configuration = [];

  /**
   * The priority.
   *
   * @var int
   */
  protected int $priority = 0;

  /**
   * Component conditions.
   *
   * @var array
   */
  protected array $conditions = [];

  /**
   * The template configuration.
   *
   * @var \TemplateConfiguration
   */
  protected TemplateConfiguration $templateConfig;

  /**
   * The plugin instance.
   *
   * @var \EntityTemplateComponentInterface
   */
  private EntityTemplateComponentInterface $_pluginInstance;

  /**
   * Construct a component configuration.
   *
   * @param string $plugin
   *   The plugin name.
   * @param array $definition
   *   The plugin definition.
   * @param array $configuration
   *   The plugin configuration.
   * @param array $conditions
   *   The plugin conditions.
   */
  public function __construct(string $plugin, array $definition = array(), array $configuration = array(), int $priority = 0, array $conditions = array()) {
    $this->plugin = $plugin;
    $this->definition = $definition;
    $this->configuration = $configuration;
    $this->priority = $priority;
    $this->conditions = $conditions;
  }

  /**
   * Get the plugin id or class.
   *
   * @return string
   *   The plugin id or class name.
   */
  public function getPluginId(): string {
    return $this->plugin;
  }

  /**
   * Get the plugin object.
   *
   * @return \EntityTemplateComponentInterface
   */
  public function getPlugin(): EntityTemplateComponentInterface {
    if (!isset($this->_pluginInstance)) {
      $this->_pluginInstance = entity_template_component_plugin_create_instance(
        $this->getPluginId(),
        $this->getTemplateConfiguration(),
        $this->getDefinition(),
        $this->getConfiguration()
      );
    }

    return $this->_pluginInstance;
  }

  /**
   * Get the definition.
   *
   * @return array
   *   The definition array.
   */
  public function getDefinition(): array {
    return $this->definition;
  }

  /**
   * Set the definition.
   *
   * @param array $definition
   *   Definition.
   *
   * @return $this
   */
  public function setDefinition(array $definition): TemplateComponentConfiguration {
    $this->definition = $definition;
    return $this;
  }

  /**
   * Get the configuration.
   *
   * @return array
   *   The configuration.
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * Set the configuration.
   *
   * @param array $configuration
   *   The new configuration.
   *
   * @return $this
   */
  public function setConfiguration(array $configuration): TemplateComponentConfiguration {
    $this->configuration = $configuration;
    return $this;
  }

  /**
   * Get the priority.
   *
   * @return int
   *   The priority. Higher priorities will be run first.
   */
  public function getPriority(): int {
    return $this->priority;
  }

  /**
   * Set the priority.
   *
   * @param int $priority
   *   The priority. Higher priorities will be run first.
   *
   * @return $this
   */
  public function setPriority(int $priority): TemplateComponentConfiguration {
    $this->priority = $priority;
    return $this;
  }

  /**
   * Get the condition.
   *
   * @return array
   *   The condtions configured for this component.
   */
  public function getConditions(): array {
    return $this->conditions;
  }

  /**
   * Set the conditions.
   *
   * @param array $conditions
   *   The conditions to set.
   *
   * @return $this
   */
  public function setConditions(array $conditions): TemplateComponentConfiguration {
    $this->conditions = $conditions;
    return $this;
  }

  /**
   * Set the template configuration.
   *
   * @param \TemplateConfiguration $template_config
   *   The template configuration this is part of.
   *
   * @return $this
   */
  public function setTemplateConfiguration(TemplateConfiguration $template_config) {
    $this->templateConfig = $template_config;
    return $this;
  }

  /**
   * Get the template configuration this is part of.
   *
   * @return \TemplateConfiguration
   */
  public function getTemplateConfiguration() : \TemplateConfiguration {
    return $this->templateConfig;
  }

  /**
   * Execute the component configuration.
   *
   * @param \EntityMetadataWrapper $structure
   *   The structure the component is being applied to.
   * @param array $parameters
   *   The parameters to component is being applied with.
   */
  public function execute(EntityMetadataWrapper $structure, array $parameters = array()) : void {
    if (class_exists(ConditionSetEvaluatorManager::class) && $this->getConditions()) {
      $conditions = $this->getConditions();

      $contexts = [];
      foreach ($parameters as $name => $parameter) {
        if ($parameter instanceof EntityDrupalWrapper) {
          $contexts[$name] = new Context(
            new ContextDefinition('entity:' . $parameter->type(), $parameter->label()),
            $parameter->value()
          );
        }
        else if ($parameter->type() === 'site') {
          $contexts[$name] = new Context(
            new ContextDefinition('site', t('Site'), FALSE),
            []
          );
        }
        else {
          $contexts[$name] = new Context(
            new ContextDefinition($parameter->type(), $parameter->label(), FALSE),
            $parameter->value()
          );
        }
      }

      $pass = (new ConditionSetEvaluatorManager())
        ->createInstance($conditions['id'], $conditions)
        ->evaluate($contexts);

      // If conditions have failed them
      if (!$pass) {
        return;
      }
    }

    $instance = entity_template_component_plugin_create_instance($this->plugin, $this->getTemplateConfiguration(), $this->definition, $this->configuration);
    $instance->apply($structure, $parameters);
  }

}
