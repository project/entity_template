<?php

use Drupal\ck_boot\Plugin\PluginFormInterface;
use Drupal\ck_opencrm\Plugin\ConditionSetEvaluator\ConditionSetEvaluatorManager;

trait TemplateComponentsFormTrait {

  protected abstract function ensureRelevantTemplateConfig(array $form, array &$form_state) : TemplateConfiguration;

  protected static abstract function &getConfigurationFormState(array $form, array &$form_state) : array;

  protected static abstract function getRootFormElement(array $element, array $form) : array;

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, array &$form_state): array {
    /** @var \EntityTemplateConfiguration $config */
    $config = $this->ensureRelevantTemplateConfig($form, $form_state);
    $config_form_state = &$this->getConfigurationFormState($form, $form_state);

    $template_wrapper_id = implode('--', array_merge($form['#parents'] ?? [], ['template-form']));
    $components_wrapper_id = implode('--', array_merge($form['#parents'] ?? [], ['template-components']));

    $form['template'] = [
      '#type' => 'container',
      '#prefix' => '<div id="' . $template_wrapper_id . '">',
      '#suffix' => '</div>',
    ];
    $form['template']['components'] = [
      '#type' => 'vertical_tabs',
      '#title' => t('Template'),
      '#template_components_root' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#prefix' => '<div id="'. $components_wrapper_id . '">',
      '#suffix' => '</div>',
      '#parents' => array_merge($form['#parents'] ?? [], ['template']),
      '#theme_wrappers' => ['vertical_tabs', 'fieldset'],
      '#tree' => TRUE,
    ];

    // Evaluator plugins.
    if (class_exists(ConditionSetEvaluatorManager::class)) {
      $evaluator_manager = new ConditionSetEvaluatorManager();
      $evaluator_plugins = [];
      foreach ($evaluator_manager->getDefinitions() as $id => $definition) {
        $evaluator_plugins[$id] = $definition['label'];
      }
    }

    $comp_plugins = entity_template_component_plugin_info();
    array_walk($comp_plugins, function (&$item, $key) use ($config) {
      $item = entity_template_component_plugin_create_instance($key, $config);
    });

    /** @var \TemplateComponentConfiguration $component */
    $configuration_state = &static::getConfigurationFormState($form, $form_state);
    $components = $config->getComponents(TRUE);
    foreach ($components as $id => $component) {
      $parents = array_merge($form['template']['components']['#parents'], [$id]);
      $form['template']['components'][$id] = [
        '#type' => 'fieldset',
        '#title' => $component->getPlugin()->label(),
        '#attributes' => [
          'class' => [
            'entity-template-component-tab',
            'entity-template-component-' . $id,
          ],
          'component' => $id,
        ],
        '#weight' => -$component->getPriority(),
        '#parents' => $parents,
        '#array_parents' => array_merge(
          $form['#array_parents'] ?? [],
          ['template', 'components', $id]
        ),
      ];

      $actions_base_name = implode('__', $parents) . '__action__';
      $form['template']['components'][$id]['actions'] = [
        '#type' => 'actions',
        '#actions_base_name' => $actions_base_name,
        'increase_priority' => [
          '#type' => 'submit',
          '#value' => decode_entities("&uArr;"),
          '#component_id' => $id,
          '#name' => $actions_base_name . 'increasepriority',
          '#limit_validation_errors' => [],
          '#validate' => [],
          '#submit' => [
            [$this, 'configurationFormSubmitIncreaseComponentPriority'],
          ],
          '#attributes' => [
            'title' => t('Increase Priority'),
          ],
          '#ajax' => [
            'callback' => [static::class, 'configurationFormAjaxCallbackRefreshComponents'],
            'wrapper' => $components_wrapper_id,
          ],
        ],
        'decrease_priority' => [
          '#type' => 'submit',
          '#value' => decode_entities("&dArr;"),
          '#component_id' => $id,
          '#name' => $actions_base_name . 'decreasepriority',
          '#limit_validation_errors' => [],
          '#validate' => [],
          '#submit' => [
            [$this, 'configurationFormSubmitDecreaseComponentPriority'],
          ],
          '#attributes' => [
            'title' => t('Decrease Priority'),
          ],
          '#ajax' => [
            'callback' => [static::class, 'configurationFormAjaxCallbackRefreshComponents'],
            'wrapper' => $components_wrapper_id,
          ],
        ],
        'remove' => [
          '#type' => 'submit',
          '#value' => t('Remove'),
          '#component_id' => $id,
          '#name' => $actions_base_name . 'remove',
          '#limit_validation_errors' => [],
          '#validate' => [],
          '#submit' => [
            [$this, 'configurationFormSubmitRemoveComponent'],
          ],
          '#ajax' => [
            'callback' => [static::class, 'configurationFormAjaxCallbackRefreshComponents'],
            'wrapper' => $components_wrapper_id,
          ],
          '#attributes' => [
            'class' => ['btn-red'],
          ],
        ],
      ];

      // Swappability
      foreach ($comp_plugins as $name => $plugin) {
        if (
          $plugin->getPluginId() !== $component->getPluginId() &&
          $plugin instanceof EntityTemplateSwappableComponentInterface &&
          $plugin->canSwapFor($component->getPlugin())
        ) {
          $form['template']['components'][$id]['actions']['swap__' . $name] = [
            '#type' => 'submit',
            '#value' => t('Switch to @label', ['@label' => $plugin->getDefinition()['label']]),
            '#component_id' => $id,
            '#swap_to' => $name,
            '#name' => $actions_base_name . 'swap__' . $name,
            '#limit_validation_errors' => [],
            '#validate' => [],
            '#submit' => [
              [$this, 'configurationFormSubmitSwapComponent'],
            ],
            '#attributes' => [
              'class' => ['btn-yellow'],
            ],
            '#ajax' => [
              'callback' => [static::class, 'configurationFormAjaxCallbackRefreshComponents'],
              'wrapper' => $components_wrapper_id,
            ],
          ];
        }
      }

      if (!empty($evaluator_plugins) && !empty($evaluator_manager)) {
        if (empty($configuration_state['component_conditions'][$id]['plugin'])) {
          $configuration_state['component_conditions'][$id]['plugin'] = $evaluator_manager->createInstance(
            $component->getConditions()['id'] ?? 'always_true',
            $component->getConditions() ?? []
          );
        }

        $form['template']['components'][$id]['conditions'] = [
          '#type' => 'fieldset',
          '#weight' => -50,
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          '#suffix' => '</div>',
          '#title' => t('Conditions'),
          '#parents' => array_merge($parents, ['conditions']),
        ];
        $conditions_wrapper_id = implode(
          '--',
          $form['template']['components'][$id]['conditions']['#parents']
        );
        $form['template']['components'][$id]['conditions']['#prefix'] = '<div id="' . $conditions_wrapper_id . '">';
        $form['template']['components'][$id]['conditions']['id'] = [
          '#type' => 'select',
          '#options' => $evaluator_plugins,
          '#default_value' => $configuration_state['component_conditions'][$id]['plugin']->getPluginId(),
          '#ajax' => [
            'callback' => [static::class, 'submitConfigurationFormPluginSelectAjaxCallback'],
            'wrapper' => $conditions_wrapper_id,
            'method' => 'replace',
            'trigger_as' => ['name' => $actions_base_name . '__update_condition'],
          ],
        ];
        $form['template']['components'][$id]['conditions']['update_plugin'] = [
          '#type' => 'submit',
          '#value' => t('Update Plugin'),
          '#name' => $actions_base_name . '__update_condition',
          '#component_id' => $id,
          '#attributes' => [
            'class' => ['js-hide'],
          ],
          '#submit' => [
            [$this, 'configurationFormSubmitConditionEvaluatorPluginSelectSubmit'],
          ],
          '#ajax' => [
            'callback' => [static::class, 'submitConfigurationFormConditionEvaluatorPluginSelectAjaxCallback'],
            'wrapper' => $conditions_wrapper_id,
            'method' => 'replace',
          ],
          '#limit_validation_errors' => [
            array_merge($form['template']['components'][$id]['conditions']['#parents'], ['id']),
          ],
        ];

        if ($configuration_state['component_conditions'][$id]['plugin'] instanceof PluginFormInterface) {
          $form['template']['components'][$id]['conditions'] = $configuration_state['component_conditions'][$id]['plugin']->buildConfigurationForm(
            $form['template']['components'][$id]['conditions'],
            $form_state
          );
        }
      }

      $form['template']['components'][$id] = $component->getPlugin()->configForm(
        $form['template']['components'][$id],
        $form_state
      );
    }

    $id = '__add_component';
    $parents = array_merge($form['template']['components']['#parents'], [$id]);
    $form['template']['components'][$id] = [
      '#type' => 'fieldset',
      '#title' => t('Add Component'),
      '#attributes' => [
        'class' => [
          'entity-template-component-tab',
          'entity-template-component-' . $id,
        ],
        'component' => $id,
      ],
      '#weight' => PHP_INT_MAX,
      '#parents' => $parents,
      '#array_parents' => array_merge(
        $form['#array_parents'] ?? [],
        ['template', 'components', $id]
      ),
      'component' => [
        '#type' => 'container',
        '#attributes' => [
          'id' => implode('--', $parents) . '--component-form',
        ],
        '#parents' => $parents,
        '#array_parents' => array_merge(
          $form['#array_parents'] ?? [],
          ['template', 'components', $id, 'component']
        ),
      ]
    ];
    if (empty($config_form_state['new_component_plugin'])) {
      $plugins = entity_template_component_plugin_info();
      foreach ($plugins as $name => $info) {
        $plugin = entity_template_component_plugin_create_instance($name, $config);
        if (!$plugin->applies($config)) {
          continue;
        }

        $form['template']['components'][$id]['component']['select_' . $name] = [
          '#type' => 'submit',
          '#value' => $info['label'],
          '#plugin_name' => $name,
          '#name' => implode('__', $parents) . '__add_select_' . $name,
          '#limit_validation_errors' => [],
          '#submit' => [
            [$this, 'configurationFormSubmitSelectNewComponentPlugin'],
          ],
          '#ajax' => $plugin instanceof EntityTemplateComponentRequiringSetupInterface && $plugin->needsSetup() ? [
            'callback' => [$this, 'configurationFormAjaxCallbackRefreshAddComponent'],
            'wrapper' => implode('--', $parents) . '--component-form',
          ] : [
            'callback' => [$this, 'configurationFormAjaxCallbackRefreshComponents'],
            'wrapper' => $components_wrapper_id,
          ],
        ];
      }
    }
    else {
      $plugin = entity_template_component_plugin_create_instance(
        $config_form_state['new_component_plugin'],
        $this->ensureRelevantTemplateConfig($form, $form_state)
      );

      $form['template']['components'][$id]['component']['setup'] = [
        '#type' => 'container',
        '#parents' => array_merge($parents, ['setup']),
        '#array_parents' => array_merge(
          $form['#array_parents'] ?? [],
          ['template', 'components', $id, 'component', 'setup']
        ),
        '#attributes' => [
          'id' => implode('--', array_merge($parents, ['setup'])). '--component-form',
        ],
      ];
      $form['template']['components'][$id]['component']['setup'] = $plugin->setupForm(
        $form['template']['components'][$id]['component']['setup'],
        $form_state
      );

      $form['template']['components'][$id]['component']['add_component'] = [
        '#type' => 'submit',
        '#value' => t('Configure Component'),
        '#plugin_name' => $config_form_state['new_component_plugin'],
        '#name' => implode('__', $parents) . '__add_component',
        '#limit_validation_errors' => [$form['template']['components'][$id]['#parents'] ?? []],
        '#validate' => [
          [$this, 'configurationFormValidateAddComponentSetup'],
        ],
        '#submit' => [
          [$this, 'configurationFormSubmitAddComponentSetup'],
        ],
        '#ajax' => [
          'callback' => [$this, 'configurationFormAjaxCallbackRefreshComponents'],
          'wrapper' => $components_wrapper_id,
        ],
      ];
    }


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationFormValidate(array $form, array &$form_state) {
    $template = $this->ensureRelevantTemplateConfig($form, $form_state);
    foreach ($template->getComponents() as $id => $component) {
      $component->getPlugin()->configFormValidate(
        $form['template']['components'][$id],
        $form_state
      );

      if (
        !empty($config_state['component_conditions'][$id]['plugin']) &&
        $config_state['component_conditions'][$id]['plugin'] instanceof PluginFormInterface
      ) {
        $config_state['component_conditions'][$id]['plugin']->validateConfigurationForm(
          $form['template']['components'][$id]['conditions'],
          $form_state
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function configurationFormSubmit(array $form, array &$form_state) {
    $config_state = &static::getConfigurationFormState($form, $form_state);

    /** @var \TemplateConfiguration $template */
    $template = $this->ensureRelevantTemplateConfig($form, $form_state);
    foreach ($template->getComponents() as $id => $component) {
      $component->getPlugin()->configFormSubmit($form['template']['components'][$id], $form_state);
      $component->setConfiguration($component->getPlugin()->getConfiguration());

      if (!empty($config_state['component_conditions'][$id]['plugin'])) {
        $conditions = [
          'id' => $config_state['component_conditions'][$id]['plugin']->getPluginId(),
        ];
        if ($config_state['component_conditions'][$id]['plugin'] instanceof PluginFormInterface) {
          $config_state['component_conditions'][$id]['plugin']->submitConfigurationForm(
            $form['template']['components'][$id]['conditions'],
            $form_state
          );
          $conditions += $config_state['component_conditions'][$id]['plugin']->getConfiguration();
        }
        $component->setConditions($conditions);
      }
    }
  }

  /**
   * Submit callback to select the new component being added.
   *
   * @param array $form
   *   The form.
   * @param array &$form_state
   *   The form state.
   */
  public function configurationFormSubmitSelectNewComponentPlugin(array $form, array &$form_state) {
    $configuration_form_state = &$this->getConfigurationFormState($form_state['triggering_element'], $form_state);

    $plugin_name = $form_state['triggering_element']['#plugin_name'];
    $plugin = entity_template_component_plugin_create_instance($plugin_name, $configuration_form_state['template_config']);
    if ($plugin instanceof EntityTemplateComponentRequiringSetupInterface && $plugin->needsSetup()) {
      $configuration_form_state['new_component_plugin'] = $plugin_name;
    }
    else {
      $components = $configuration_form_state['template_config']->getComponents(TRUE);

      $new_component = new TemplateComponentConfiguration($plugin_name);
      $new_component->setTemplateConfiguration($configuration_form_state['template_config']);
      $new_component->setPriority(count($components) ? end($components)->getPriority() - 1 : 20);
      $configuration_form_state['template_config']->addComponent($new_component);
    }

    $form_state['rebuild'] = TRUE;
  }

  /**
   * Submit callback for adding a component to the template config.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return void
   */
  public function configurationFormValidateAddComponentSetup(array $form, array &$form_state) {
    // Locate the form section that is relevant.
    $array_parents = array_slice($form_state['triggering_element']['#array_parents'], 0, -1);
    $plugin_form = drupal_array_get_nested_value($form, array_merge($array_parents, ['setup']));

    $configuration_form_state = &$this->getConfigurationFormState($form_state['triggering_element'], $form_state);
    $plugin_name = $configuration_form_state['new_component_plugin'];
    /** @var \EntityTemplateComponentInterface $plugin */
    $plugin = entity_template_component_plugin_create_instance($plugin_name, $configuration_form_state['template_config']);
    $plugin->setupFormValidate($plugin_form, $form_state);
  }

  /**
   * Submit callback for adding a component to the template config.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return void
   */
  public function configurationFormSubmitAddComponentSetup(array $form, array &$form_state) {
    // Locate the form section that is relevant.
    $array_parents = array_slice($form_state['triggering_element']['#array_parents'], 0, -1);
    $plugin_form = drupal_array_get_nested_value($form, array_merge($array_parents, ['setup']));

    $configuration_form_state = &$this->getConfigurationFormState($form_state['triggering_element'], $form_state);
    $plugin_name = $configuration_form_state['new_component_plugin'];
    /** @var \EntityTemplateComponentInterface $plugin */
    $plugin = entity_template_component_plugin_create_instance($plugin_name, $configuration_form_state['template_config']);
    $plugin->setupFormSubmit($plugin_form, $form_state);

    $components = $configuration_form_state['template_config']->getComponents(TRUE);
    $new_component = new TemplateComponentConfiguration($plugin_name, $plugin->getDefinition());
    $new_component->setTemplateConfiguration($configuration_form_state['template_config']);
    $new_component->setPriority(count($components) ? end($components)->getPriority() - 1 : 20);
    $configuration_form_state['template_config']->addComponent($new_component);
    unset($configuration_form_state['new_component_plugin']);

    $form_state['rebuild'] = TRUE;
  }

  /**
   * Submit callback to remove component.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return void
   */
  public function configurationFormSubmitRemoveComponent($form, &$form_state) {
    $button = $form_state['triggering_element'];
    $configuration_form_state = &$this->getConfigurationFormState($form_state['triggering_element'], $form_state);
    $template_config = $configuration_form_state['template_config'];
    $template_config->removeComponent($button['#component_id']);

    $form_state['rebuild'] = TRUE;
  }

  /**
   * Increase a component's priority.
   *
   * @param $form
   * @param $form_state
   *
   * @return void
   */
  public function configurationFormSubmitIncreaseComponentPriority($form, &$form_state) {
    $button = $form_state['triggering_element'];
    $configuration_form_state = &$this->getConfigurationFormState($form_state['triggering_element'], $form_state);
    /** @var \TemplateConfiguration $template_config */
    $template_config = $configuration_form_state['template_config'];

    // I considered an algorithm here that would make the priority the middle of the
    // two priorities above it, but there were lots of awkward cases. In this version
    // we just make sure that the new priority is at least one more than the priority
    // of the previous component so it will always jump atleast one step.
    $last_component = NULL;
    foreach ($template_config->getComponents(TRUE) as $id => $component) {
      if ($id === $button['#component_id']) {
        $component->setPriority(($last_component ? $last_component->getPriority() : $component->getPriority()) + 1);
        break;
      }

      $last_component = $component;
    }

    $form_state['rebuild'] = TRUE;
  }

  /**
   * Decrease a component's priority.
   *
   * @param $form
   * @param $form_state
   *
   * @return void
   */
  public function configurationFormSubmitDecreaseComponentPriority($form, &$form_state) {
    $button = $form_state['triggering_element'];
    $configuration_form_state = &$this->getConfigurationFormState($form_state['triggering_element'], $form_state);
    /** @var \TemplateConfiguration $template_config */
    $template_config = $configuration_form_state['template_config'];

    // I considered an algorithm here that would make the priority the middle of the
    // two priorities above it, but there were lots of awkward cases. In this version
    // we just make sure that the new priority is at least one more than the priority
    // of the previous component so it will always jump atleast one step.
    $last_component = NULL;
    foreach (array_reverse($template_config->getComponents(TRUE)) as $id => $component) {
      if ($id === $button['#component_id']) {
        $component->setPriority(($last_component ? $last_component->getPriority() : $component->getPriority()) - 1);
        break;
      }

      $last_component = $component;
    }

    $form_state['rebuild'] = TRUE;
  }

  /**
   * Submit callback to swap a component.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return void
   */
  public function configurationFormSubmitSwapComponent($form, &$form_state) {
    $button = $form_state['triggering_element'];
    $configuration_form_state = &$this->getConfigurationFormState($form_state['triggering_element'], $form_state);
    /** @var \TemplateConfiguration $template_config */
    $template_config = $configuration_form_state['template_config'];

    /** @var \EntityTemplateSwappableComponentInterface $swap */
    $swap = entity_template_component_plugin_create_instance($button['#swap_to'], $template_config);

    $existing = $template_config->getComponent($button['#component_id']);
    $replacement = new TemplateComponentConfiguration(
      $swap->getPluginId(),
      $swap->prepareSwapDefinitionFor($existing->getPlugin()),
      $swap->prepareSwapConfigurationFor($existing->getPlugin()),
      $existing->getPriority(),
      $existing->getConditions()
    );
    $template_config->replaceComponent($button['#component_id'], $replacement);

    $form_state['rebuild'] = TRUE;
  }

  /**
   * The configuration form submit callback that changes the evaluator plugins.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function configurationFormSubmitConditionEvaluatorPluginSelectSubmit($form, &$form_state) {
    $button = $form_state['triggering_element'];
    $configuration_state = &static::getConfigurationFormState($button, $form_state);

    $parents = $button['#parents']; array_pop($parents); array_push($parents, 'id');
    $plugin_id = drupal_array_get_nested_value($form_state['values'], $parents);
    $plugin_manager = new ConditionSetEvaluatorManager();

    $configuration_state['component_conditions'][$button['#component_id']]['plugin'] = $plugin_manager->createInstance($plugin_id);
    $form_state['rebuild'] = TRUE;
  }

  /**
   * Refresh the components table.
   *
   * @param array $form
   * @param array $form_state
   */
  public static function configurationFormAjaxCallbackRefreshComponents(array $form, array $form_state) {
    return static::getRootFormElement($form_state['triggering_element'], $form)['template']['components'];
  }

  /**
   * Refresh the components table.
   *
   * @param array $form
   * @param array $form_state
   */
  public static function configurationFormAjaxCallbackRefreshAddComponent(array $form, array $form_state) {
    return static::getRootFormElement($form_state['triggering_element'], $form)['template']['components']['__add_component']['component'];
  }

  /**
   * Ajax callback for refreshing the conditions section.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return array
   */
  public static function submitConfigurationFormConditionEvaluatorPluginSelectAjaxCallback($form, $form_state) {
    $root_element = static::getRootFormElement($form_state['triggering_element'], $form);
    $component_id = $form_state['triggering_element']['#component_id'];

    return $root_element['template']['components'][$component_id]['conditions'];
  }

}
