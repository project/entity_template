<?php
/**
 * @file
 *   Contains entity class for entity templates.
 */

/**
 * Interface for things that can be used as entity templates.
 */
interface EntityTemplateInterface {

  /**
   * Get the api version.
   *
   * @return int
   */
  public function apiVersion() : int;

  /**
   * Get the label of this entity template.
   *
   * @return string
   */
  public function label();

  /**
   * Get the identifier of this template.
   *
   * @return string
   */
  public function identifier();

  /**
   * Get the builder.
   *
   * @return \EntityTemplateBuilderInterface
   */
  public function getBuilder() : EntityTemplateBuilderInterface;

  /**
   * Get the target entity type.
   *
   * @return string
   */
  public function getTargetEntityType() : ?string;

  /**
   * Get the target bundle.
   *
   * @return string
   */
  public function getTargetBundle() : ?string;

  /**
   * Get the parameters info.
   *
   * @return array
   */
  public function getParameters() : array;

  /**
   * Get an array of template information.
   *
   * @return array|null
   */
  public function getTemplate() : ?array;

  /**
   * Set the template configuration.
   *
   * @param array|string
   *   The template configuration.
   */
  public function setTemplate($configuration);

  /**
   * Create a new entity from the template using the provided parameters.
   *
   * @param array $parameters
   *   The parameters
   *
   * @return object
   *   The entity.
   */
  public function createNewEntityFromTemplate(array $parameters = []) : object;

  /**
   * Create a new entity from the template using the provided parameters.
   *
   * @param \EntityDrupalWrapper $entity
   *   The entity to apply the template to.
   * @param array $parameters
   *   The parameters
   *
   * @return object
   *   The entity.
   */
  public function applyTemplateToEntity(EntityDrupalWrapper $entity, array $parameters = []) : object;

}

/**
 * Trait for common entity template methods.
 */
trait EntityTemplateTrait {

  /**
   * Get the builder.
   *
   * @return \EntityTemplateBuilderInterface
   *   The builder that will execute this template.
   */
  public function getBuilder() : EntityTemplateBuilderInterface {
    $class = $this->apiVersion() == 2 ? ComponentEntityTemplateBuilder::class : OriginalEntityTemplateBuilder::class;
    return new $class($this);
  }

  /**
   * Prepare the provided parameters.
   *
   * @param array $parameters
   *   The parameters.
   *
   * @return array
   */
  protected function prepareParameters(array $parameters = []) : array {
    // Get the parameter info off the template.
    $parameter_info = $this->getParameters();

    // Make sure params are wrapped.
    $wrapped_params = array();
    foreach ($parameters as $namespace => $param) {
      if ($param instanceof EntityMetadataWrapper) {
        $wrapped_params[$namespace] = $param;
      }
      else {
        $info = $parameter_info[$namespace];
        $wrapped_params[$namespace] = entity_metadata_wrapper($info['type'], $param, $info);
      }
    }

    return $wrapped_params;
  }

  /**
   * {@inheritdoc}
   */
  public function createNewEntityFromTemplate(array $parameters = []) : object {
    return $this->getBuilder()->executeTemplate($this->prepareParameters($parameters));
  }

  /**
   * {@inheritdoc}
   */
  public function applyTemplateToEntity(EntityDrupalWrapper $entity, array $parameters = []) : object {
    return $this->getBuilder()->executeTemplate($this->prepareParameters($parameters), $entity);
  }
}

/**
 * Template entity class.
 */
class EntityTemplate extends Entity implements EntityTemplateInterface {
  use EntityTemplateTrait;

  /**
   * The api version this template is using.
   *
   * Defaults to version 1.
   *
   * @var int
   */
  public $api = 1;

  /**
   * The human readable label of the template.
   * @var string
   */
  public $label;

  /**
   * The unique string identifier.
   * @var string
   */
  public $name;

  /**
   * The uuid of the entity template.
   *
   * @var string
   */
  public $uuid;

  /**
   * The entity type this template is for.
   * @var string
   */
  public $entity_type;

  /**
   * The bundle of the entity this is for.
   * @var string
   */
  public $bundle;

  /**
   * The parameters needed to create an entity from this template.
   * @var Array
   */
  public $parameters = array();

  /**
   * The template.
   * @var string
   */
  public $template;

  /**
   * {@inheritdoc}
   */
  public function apiVersion() : int {
    return $this->api;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityType(): string {
    return $this->entity_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetBundle(): string {
    return $this->bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplate(): ?array {
    return is_string($this->template) ? drupal_json_decode($this->template) : ($this->template ?: NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function setTemplate($configuration) {
    $this->template = is_string($configuration) ? $configuration : drupal_json_encode($configuration);
  }

  /**
   * Get the parameters.
   *
   * Use this to add the site settings parameter info to all templates.
   */
  function getParameters() : array {
    return $this->parameters + array(
      'site' => array(
        'type' => 'site',
        'label' => t('Site Information'),
        'description' => t('Site-wide settings and other global information.'),
        'property info alter' => array('RulesData', 'addSiteMetadata'),
        'property info' => array(),
        'optional' => TRUE,
      ),
      'self' => array(
        'label' => t('Entity being Created'),
        'description' => t('The entity being created by this template.'),
        'type' => $this->entity_type,
        'bundle' => $this->bundle,
        'optional' => TRUE,
      )
    );
  }

  /**
   * Build the template.
   */
  function buildTemplate() {
    return $this->getBuilder()->buildTemplate();
  }

  /**
   * Get the uuid of the entity template.
   *
   * @return string
   *   The uuid.
   */
  public function uuid() : string {
    if (empty($this->uuid)) {
      $this->uuid = uuid_generate();
    }

    return $this->uuid;
  }

}

/**
 * Entity Controller
 */
class EntityTemplateController extends EntityAPIControllerExportable {

  /**
   * {@inheritdoc}
   */
  public function export($entity, $prefix = '') {
    $tmpEntity = clone $entity;
    $tmpEntity->template = drupal_json_decode($tmpEntity->template);

    $vars = get_object_vars($tmpEntity);
    unset($vars[$this->statusKey], $vars[$this->moduleKey], $vars['is_new']);
    if ($this->nameKey != $this->idKey) {
      unset($vars[$this->idKey]);
    }

    return static::exportVarJsonExport($vars, $prefix);
  }

  /**
   * Process export json.
   *
   * @param $var
   *   The variable to be json encoded.
   * @param $prefix
   *   The prefix.
   *
   * @return string
   *   A portion of encoded json.
   */
  public static function exportVarJsonExport($var, $prefix = '') {
    if (is_array($var) && $var) {
      // Defines whether we use a JSON array or object.
      $use_array = ($var == array_values($var));
      $output = $use_array ? "[" : "{";

      foreach ($var as $key => $value) {
        if ($use_array) {
          $values[] = static::exportVarJsonExport($value, '  ');
        }
        else {
          $values[] = static::exportVarJsonExport((string) $key, '  ') . ' : ' . static::exportVarJsonExport($value, '  ');
        }
      }
      // Use several lines for long content. However for objects with a single
      // entry keep the key in the first line.
      if (strlen($content = implode(', ', $values)) > 70 && ($use_array || count($values) > 1)) {
        $output .= "\n  " . implode(",\n  ", $values) . "\n";
      }
      elseif (strpos($content, "\n") !== FALSE) {
        $output .= " " . $content . "\n";
      }
      else {
        $output .= " " . $content . ' ';
      }
      $output .= $use_array ? ']' : '}';
    }
    elseif (is_string($var)) {
      if (stripos($var, "\n") !== FALSE) {
        $var = str_replace("\r\n", "\n", $var, $count);
        $new_var = array(
          'FN::Implode' => explode("\n", $var),
          'FN::Implode::Separator' => $count ? '\r\n' : '\n',
        );
        $output = static::exportVarJsonExport($new_var, '  ');
      }
      else {
        $output = json_encode($var, JSON_HEX_QUOT);
      }
    }
    else {
      $output = drupal_json_encode($var);
    }

    if ($prefix) {
      $output = str_replace("\n", "\n$prefix", $output);
    }

    return $output;
  }

  /**
   * Process json export being imported.
   *
   * @param $var
   *   The decoded values.
   *
   * @return mixed
   *   Processed values.
   */
  public static function importVarJsonImport($var) {
    if (is_array($var)) {
      foreach ($var as $key => $value) {
        if ($key === 'FN::Implode') {
          return implode(
            str_replace(array('\r\n', '\n'), array("\r\n", "\n"), $var['FN::Implode::Separator'] ?? '\n'),
            $value
          );
        }
        else {
          $var[$key] = static::importVarJsonImport($value);
        }
      }
    }

    return $var;
  }

  /**
   * {@inheritdoc}
   */
  public function import($export) {
    $vars = static::importVarJsonImport(drupal_json_decode($export));

    if (is_array($vars)) {
      $entity = $this->create($vars);
    }
    else {
      return FALSE;
    }

    if (!is_string($entity->template)) {
      $entity->template = drupal_json_encode($entity->template);
    }

    unset($entity->entity_template_tags);
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function invoke($hook, $entity) {
    if (in_array($hook, array('insert', 'update'))) {
      $this->storeTags($entity);
    }

    if ($hook == 'update') {
      $this->purgeRevisions($entity);
    }

    parent::invoke($hook, $entity);

    // Invoke rules.
    if (module_exists('rules')) {
      rules_invoke_event($this->entityType . '_' . $hook, $entity);
    }
  }

  /**
   * Purge older revisions from the object.
   */
  public function purgeRevisions($template) {
    if ($max_historical_revisions = variable_get('entity_template_max_revisions', NULL)) {
      $vids = db_select('entity_template_revision', 'r')
        ->fields('r', array('vid'))
        ->condition('r.id', $template->id)
        ->condition('r.vid', $template->vid, '<>')
        ->orderBy('r.timestamp', 'DESC')
        ->orderBy('r.vid', 'DESC')
        ->range($max_historical_revisions, 1000)
        ->execute()
        ->fetchCol();

      foreach ($vids as $vid) {
        entity_revision_delete('entity_template', $vid);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function attachLoad(&$queried_entities, $revision_id = FALSE) {
    parent::attachLoad($queried_entities, $revision_id);

    $query = db_select('entity_template_tags', 't')
      ->fields('t', array('id', 'tag'));
    $or = db_or();
    foreach ($queried_entities as $id => $queried_entity) {
      $or->condition(db_and()
        ->condition('t.id', $queried_entity->id)
        ->condition('t.vid', $queried_entity->vid)
      );
    }
    $query->condition($or);
    foreach ($query->execute() as $row) {
      $tags[$row->id][] = $row->tag;
    }

    foreach ($queried_entities as &$queried_entity) {
      $queried_entity->tags = isset($tags[$queried_entity->id]) ? $tags[$queried_entity->id] : array();
    }
  }

  /**
   * Store the tags.
   */
  public function storeTags($entity) {
    db_delete('entity_template_tags')
      ->condition('id', $entity->id)
      ->condition('vid', $entity->vid)
      ->execute();
    if (!empty($entity->tags)) {
      $query = db_insert('entity_template_tags')->fields(array('id', 'vid', 'tag'));
      foreach ($entity->tags as $tag) {
        $query->values(array(
          'id' => $entity->id,
          'vid' => $entity->vid,
          'tag' => $tag,
        ));
      }
      $query->execute();
    }
  }

}
