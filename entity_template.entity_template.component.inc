<?php

/**
 * Implements hook_entity_template_component_plugin_info().
 */
function entity_template_entity_template_component_plugin_info() {
  $info = [];
  $info['input_value'] = [
    'class' => InputValueEntityTemplateComponent::class,
    'label' => t('Direct Input'),
  ];
  $info['data_select'] = [
    'class' => DataSelectEntityTemplateComponent::class,
    'label' => t('Select Value(s)'),
  ];
  $info['nested_structure'] = [
    'class' => NestedStructureTemplateEntityTemplateComponent::class,
    'label' => t('Sub-Template'),
  ];
  return $info;
}

/**
 * Implements hook_entity_template_component_modifier_plugin_info().
 */
function entity_template_entity_template_component_modifier_plugin_info() {
  $info = [];
  $info['ctools_tokens'] = [
    'class' => CtoolsTokensComponentModifier::class,
    'singleton' => TRUE,
    'label' => t('CTools Tokens'),
    'description' => t('CTools Tokens will be replaced in this value. Start your token with @:.'),
  ];
  $info['prevent_duplicate_entities'] = [
    'class' => PreventDuplicateEntitiesComponentModifier::class,
    'singleton' => TRUE,
    'label' => t('Prevent Duplicate Entities'),
    'description' => t('The database will be searched for a matching entity and that will be used instead.'),
  ];
  return $info;
}
