<?php
/**
 * @file
 *   Plugin for creating entities from templates.
 */

/**
 * The plugin defnition.
 */
$plugin = array(
  'title' => t('Create Entity from Template'),
  'defaults' => array(),
  'content_type' => 'entity_template_entity_create_content_type_content_type',
);

/**
 * Get one subtype ofr this content type.
 */
function entity_template_entity_create_content_type_content_type($subtype) {
  $types = entity_template_entity_create_content_type_content_types();
  if (isset($types[$subtype])) {
    return $types[$subtype];
  }
}

/**
 * Return all entity create pane content types.
 */
function entity_template_entity_create_content_type_content_types() {
  $types = &drupal_static(__FUNCTION__, array());
  if (!empty($types)) {
    return $types;
  }

  $defaults = array(
    'category' => t('Entity Templates'),
    'title' => t('Create Entity From Template'),
    'description' => t('Show a form to create an entity from a template.'),
    'edit form' => 'entity_template_entity_create_content_type_options',
  );

  $types['__select'] = $defaults;

  foreach (entity_load('entity_template') as $template) {
    $types[$template->name] = $defaults;
    $types[$template->name]['title'] .= " {$template->label}";
    $types[$template->name]['template'] = $template->name;

    if (!empty($template->parameters)) {
      $types[$template->name.'__context'] = $types[$template->name];
      $types[$template->name.'__context']['title'] .= " (Parameter Contexts)";

      $einfo = entity_get_info();
      foreach ($template->parameters as $name => $param) {
        $type = (isset($einfo[$param['type']])) ? 'entity:'.$param['type'] : $param['type'];

        if (!empty($param['optional'])) {
          $context = new ctools_context_optional($param['label'], $type);
        }
        else {
          $context = new ctools_context_required($param['label'], $type);
        }

        // @todo: Work out how to support optional parameters.
        $types[$template->name.'__context']['required context'][$name] = $context;
      }
    }
  }

  return $types;
}

/**
 * Options form for the pane.
 */
function entity_template_entity_create_content_type_options($form, &$form_state) {
  return $form;
}

/**
 * Options form submit.
 */
function entity_template_entity_create_content_type_options_submit($form, &$form_state) {}

/**
 * Returns the admin title for the pane.
 */
function entity_template_entity_create_content_type_admin_title($subtype, $conf, $context) {
  $title = t("Entity Create From Template Form");
  $plugin = entity_template_entity_create_content_type_content_type($subtype);
  if (!empty($plugin['template'])) {
    $template = entity_load_single('entity_template', $plugin['template']);
    $title .= ' ('.$template->label.')';
  }
  return $title;
}

/**
 * Renders the pane.
 */
function entity_template_entity_create_content_type_render($subtype, $conf, $contexts) {
  $template = NULL;
  $parameters = array();

  if ($subtype != '__select') {
    $plugin = entity_template_entity_create_content_type_content_type($subtype);
    $template = entity_load_single('entity_template', $plugin['template']);

    if (substr($subtype, -9) == '__context') {
      foreach ($contexts as $name => $context) {
        $parameters[$name] = $context->data;
      }
    }
  }

  $title = t("Create Entity");
  $content = drupal_get_form('entity_template_entity_create_form', $template, $parameters);

  // Build the content type block.
  $block = new stdClass();
  $block->module  = 'entity_template';
  $block->title = $title;
  $block->content = $content;
  $block->delta   = $subtype;

  return $block;
}
