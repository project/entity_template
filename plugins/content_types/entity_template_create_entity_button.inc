<?php
/**
 * @file
 *   Plugin for creating entities from templates.
 */

/**
 * The plugin defnition.
 */
$plugin = array(
  'title' => t('Create Entity from Template Button'),
  'defaults' => array(),
  'content_type' => 'entity_template_entity_template_create_entity_button_content_type_content_type',
  'all contexts' => TRUE,
);

/**
 * Get one subtype ofr this content type.
 */
function entity_template_entity_template_create_entity_button_content_type_content_type($subtype) {
  $types = entity_template_entity_template_create_entity_button_content_type_content_types();
  if (isset($types[$subtype])) {
    return $types[$subtype];
  }
}

/**
 * Return all entity create pane content types.
 */
function entity_template_entity_template_create_entity_button_content_type_content_types() {
  $types = &drupal_static(__FUNCTION__, array());
  if (!empty($types)) {
    return $types;
  }

  $defaults = array(
    'category' => t('Entity Templates'),
    'title' => t('Create Entity from Template Button'),
    'description' => t('Show a button to create a template.'),
    'edit form' => 'entity_template_entity_template_create_entity_button_content_type_options',
    'all contexts' => TRUE,
  );

  $types['select'] = $defaults;
  return $types;
}

/**
 * Options form for the pane.
 */
function entity_template_entity_template_create_entity_button_content_type_options($form, &$form_state) {
  $form['link_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Text'),
    '#default_value' => $form_state['conf']['link_text'],
  );

  $form['link_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Button Classes'),
    '#default_value' => $form_state['conf']['link_class'],
  );

  $template_options = array();
  foreach(entity_load('entity_template') as $template) {
    $template_options[$template->name] = $template->label;
  }
  $form['template'] = array(
    '#type' => 'select',
    '#title' => t('Template'),
    '#default_value' => $form_state['conf']['template'],
    '#required' => TRUE,
    '#options' => $template_options,
  );

  $parameters_value = array();
  if (!empty($form_state['conf']['parameters'])) {
    foreach ($form_state['conf']['parameters'] as $name => $value) {
      $parameters_value[] = "{$name}|{$value}";
    }
  }
  $parameters_value = implode("\n", $parameters_value);
  $form['parameters'] = array(
    '#type' => 'textarea',
    '#title' => t('Parameters'),
    '#description' => t('Add each parameter on a new line in the form name|value.'),
    '#default_value' => $parameters_value,
  );

  return $form;
}

/**
 * Options form submit.
 */
function entity_template_entity_template_create_entity_button_content_type_options_submit($form, &$form_state) {
  $form_state['conf']['link_text'] = !empty($form_state['values']['link_text']) ? $form_state['values']['link_text'] : NULL;
  $form_state['conf']['link_class'] = !empty($form_state['values']['link_class']) ? $form_state['values']['link_class'] : NULL;
  $form_state['conf']['template'] = $form_state['values']['template'];

  $parameters_value = $form_state['values']['parameters'];
  $list = explode("\n", $parameters_value);
  $list = array_map('trim', $list);
  $list = array_filter($list, 'strlen');

  foreach ($list as $string) {
    list($key, $value) = explode('|', $string, 2);
    $form_state['conf']['parameters'][$key] = $value;
  }
}

/**
 * Returns the admin title for the pane.
 */
function entity_template_entity_template_create_entity_button_content_type_admin_title($subtype, $conf, $context) {
  return t('Create Entity from Template Button');
}

/**
 * Renders the pane.
 */
function entity_template_entity_template_create_entity_button_content_type_render($subtype, $conf, $args, $contexts) {
  $template = NULL;
  $parameters = array();

  if (!empty($conf['parameters'])) {
    foreach ($conf['parameters'] as $key => $value) {
      $parameters[$key] = ctools_context_keyword_substitute($value, array(), $contexts);
    }
  }

  $query = $parameters;
  $query = array_filter($query);

  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();

  $title = t("Create an Entity From Template");
  $content = ctools_modal_text_button(
    !empty($conf['link_text']) ? $conf['link_text'] : 'Create',
    url('template/add/'.$conf['template'].'/nojs', array('absolute' => TRUE, 'query' => $query)),
    $title,
    $conf['link_class']
  );

  // Build the content type block.
  $block = new stdClass();
  $block->module  = 'entity_template';
  $block->title = $title;
  $block->content = $content;
  $block->delta   = $subtype;

  return $block;
}
