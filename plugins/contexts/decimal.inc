<?php

/**
 * @file
 * Plugin to provide a decimal context.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Decimal'),
  'description' => t('A context that is just a decimal.'),
  'context' => 'entity_template_context_create_decimal',
  'edit form' => 'entity_template_context_decimal_settings_form',
  'defaults' => '',
  'keyword' => 'decimal',
  'no ui' => FALSE,
  'context name' => 'decimal',
  'convert list' => array(
    'raw' => t('Raw decimal'),
    'format' => t('Formatted decimal'),
    'ordinal' => t('Ordinal Number (e.g. 1st)'),
    'spellout' => t('Spelled Number (e.g. One)'),
  ),
  'convert' => 'entity_template_context_decimal_convert',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the decimal for this context.'),
  ),
);

/**
 * It's important to remember that $conf is optional here, because contexts
 * are not always created from the UI.
 */
function entity_template_context_create_decimal($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('decimal');
  $context->plugin = 'decimal';

  if ($empty) {
    return $context;
  }

  if ($data !== FALSE) {
    // Support the array storage from the settings form but also handle direct input from arguments.
    $context->data = is_array($data) ? $data['decimal'] : $data;
    $context->title = ($conf) ? check_plain($data['identifier']) : check_plain($data);
    return $context;
  }
}

/**
 * Convert a context into a string.
 */
function entity_template_context_decimal_convert($context, $type) {
  list($type, $details) = explode(':', $type, 2);

  switch ($type) {
    case 'raw':
      return $context->data;

    case 'format':
      $details = explode(':', $details);
      return number_format($context->data, $details[0] ?? 0, $details[1] ?? '.', $details[2] ?? ',');

    case 'ordinal':
      $formatter = NumberFormatter::create('en_US', NumberFormatter::ORDINAL);
      return $formatter->format($context->data);

    case 'spellout':
      $formatter = NumberFormatter::create('en_US', NumberFormatter::SPELLOUT);
      return $formatter->format($context->data);
  }
}

/**
 * decimal settings form.
 */
function entity_template_context_decimal_settings_form($form, &$form_state) {
  $conf = &$form_state['conf'];

  $form['decimal'] = array(
    '#title' => t('Enter the decimal'),
    '#type' => 'textfield',
    '#maxlength' => 15,
    '#weight' => -10,
    '#default_value' => $conf['decimal'],
  );

  return $form;
}

function entity_template_context_decimal_settings_form_submit($form, &$form_state) {
  $form_state['conf']['decimal'] = $form_state['values']['decimal'];
}
