<?php

/**
 * @file
 * Plugin to provide a date context.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Date'),
  'description' => t('A context that reprensents a date or datetime.'),
  'context' => 'entity_template_context_create_date',
  'edit form' => 'entity_template_context_date_settings_form',
  'defaults' => '',
  'keyword' => 'date',
  'no ui' => FALSE,
  'context name' => 'date',
  'convert list' => array(
    'format' => t('Formatted Date'),
    'custom_format' => t('Custom Formatted Date')
  ),
  'convert' => 'entity_template_context_date_convert',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter date for this content.'),
  ),
);

/**
 * It's important to remember that $conf is optional here, because contexts
 * are not always created from the UI.
 */
function entity_template_context_create_date($empty, $data = NULL, $conf = FALSE) {
  // The input is expected to be an object as created by ctools_break_phrase
  // which contains a group of string.
  $context = new ctools_context('date');
  $context->plugin = 'date';

  if ($empty) {
    return $context;
  }

  if ($data !== FALSE) {
    if (is_array($data)) {
      $data = $data['date'];
    }

    $context->data = $data instanceof DateTimeInterface ?
      $data :
      new DateTime(is_numeric($data) ? '@' . $data : $data);
    $context->title = ($conf) ? check_plain($data['identifier']) : t('Date');
    return $context;
  }
}

/**
 * Convert a context into a string.
 */
function entity_template_context_date_convert($context, $type) {
  list($type, $details) = explode(':', $type, 2);

  switch ($type) {
    case 'format':
      return format_date($context->data->getTimestamp(), $details, '', date_default_timezone());
    case 'custom_format':
      return $context->data->setTimezone(new DateTimeZone(date_default_timezone()))->format($details);
  }
}

/**
 * String settings form.
 */
function entity_template_context_date_settings_form($form, &$form_state) {
  $conf = &$form_state['conf'];

  $form['date'] = array(
    '#title' => t('Enter the date'),
    '#type' => 'textfield',
    '#maxlength' => 512,
    '#weight' => -10,
    '#default_value' => $conf['date'],
  );

  return $form;
}

function entity_template_context_date_settings_form_submit($form, &$form_state) {
  $form_state['conf']['date'] = $form_state['values']['date'];
}
