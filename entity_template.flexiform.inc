<?php
/**
 * @file
 *   Flexiform hooks for entity template.
 */

/**
 * Implements hook_flexiform_group_info().
 */
function entity_template_flexiform_group_info() {
  return array(
    'entity_template' => array(
      'label' => t('Template Forms'),
    ),
  );
}

/**
 * Implements hook_flexiform_entity_getter_info().
 */
function entity_template_flexiform_entity_getter_info() {
  $getters = array();

  foreach (entity_load('entity_template') as $template) {
    $getter_name = 'entity_template__'.$template->name;
    $getters[$getter_name] = array(
      'label' => t('Entity Template: !label', array('!label' => $template->label)),
      'description' =>  t('Create a new entity from template.'),
      'entity_types' => array($template->entity_type),
      'entity_template' => $template->name,
      'class' => 'EntityTemplateFormEntityCreateFromTemplate',
    );

    $params = array();
    foreach ($template->parameters as $name => $param) {
      if (!entity_get_info($param['type'])) {
        continue;
      }

      $params[$name] = array(
        'entity_type' => $param['type'],
      );
    }

    if (!empty($params)) {
      $getters[$getter_name]['params'] = $params;
    }
  }

  return $getters;
}
