<?php

class OriginalEntityTemplateBuilder extends EntityTemplateBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function buildTemplate() : object {
    if (empty($this->template->getTemplate())) {
      return $this->prepareBlankTemplate();
    }

    // Build the template entity. This is almost the same as calling 'import'
    // but we pass the template in to the create method as well.
    $vars = $this->template->getTemplate();
    if (is_array($vars)) {
      $vars['__template'] = $this->template;
      $entity = entity_get_controller($this->template->getTargetEntityType())->create($vars);
    }

    if (empty($entity)) {
      drupal_set_message(t('Could not create entity from template.'), 'warning');
      return $this->prepareBlankTemplate();
    }

    // Find field collection items and tweak the entity array.
    // Move this into a hook/make more generically pluggable.
    foreach ($entity as $prop_name => &$prop) {
      if (($field = field_info_field($prop_name)) && $field['type'] == 'field_collection') {
        foreach ($prop as $langcode => &$items) {
          if (!is_array($items)) {
            continue;
          }

          foreach ($items as $delta => &$item) {
            if (!empty($item['entity']) && is_array($item['entity'])) {
              $item['entity'] = entity_create('field_collection_item', $item['entity']);
            }
          }
        }
      }
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function executeTemplate(array $parameters, EntityDrupalWrapper $target = NULL): object {
    if (!is_null($target)) {
      throw new \Exception('Applying to an existing entity in version 1 templates is not supported.');
    }

    $entity = $this->buildTemplate();
    $wrapper = entity_metadata_wrapper($this->template->getTargetEntityType(), $entity);

    // Get the parameter info off the template..
    $parameter_info = $this->template->getParameters();

    // Make sure params are wrapped.
    $wrapped_params = array();
    foreach ($parameters as $namespace => $param) {
      if ($param instanceof EntityMetadataWrapper) {
        $wrapped_params[$namespace] = $param;
        $parameters[$namespace] = $param->value();
      }
      else {
        $info = $parameter_info[$namespace];
        $wrapped_params[$namespace] = entity_metadata_wrapper($info['type'], $param);
      }
    }

    // Add Site Params.
    if (empty($wrapped_params['site']) && isset($parameter_info['site'])) {
      $wrapped_params['site'] = entity_metadata_wrapper('site', FALSE, $parameter_info['site']);
    }
    // Add Self Param.
    if (empty($wrapped_params['self'])) {
      $wrapped_params['self'] = entity_metadata_wrapper($this->template->getTargetEntityType(), $entity, $this->template->getParameters()['self']);
    }

    // Loop over properties and convert data selections.
    $properties = $wrapper->getPropertyInfo();
    foreach ($entity as $prop => $value) {
      if (!is_array($value) || empty($properties[$prop])) {
        continue;
      }

      // Set the value to null.
      $entity->{$prop} = NULL;

      // Work out what method to use.
      $method = !empty($value['select']) ? 'select' : 'direct';
      if (!empty($value['method'])) {
        $method = $value['method'];
      }

      $property = $properties[$prop];
      if ($method == 'sub_select') {
        if (!is_array(reset($value['sub_select']))) {
          // If the first item isn't an array then we are dealing with a pre
          // update template. Include this code for backwards compatibility.
          foreach (array_filter($value['sub_select']) as $sub_prop => $selector) {
            // As options are stored in the same way as sub selectors we check
            // that the sub property exists.
            if (empty($property['property info'][$sub_prop])) {
              continue;
            }

            // Apply the selector.
            $processors = NULL;
            if (!empty($value['sub_select'][$sub_prop.'_template_options'])) {
              $processors = $value['sub_select'][$sub_prop.'_template_options'];
              RulesDataProcessor::prepareSetting(
                $processors,
                $property['property info'][$sub_prop],
                $parameter_info
              );
            }
            $prop_wrapper = $wrapper->{$prop};
            if ($prop_wrapper instanceof EntityListWrapper) {
              $prop_wrapper = $prop_wrapper[0];
            }

            $this->applyDataSelector($prop_wrapper->{$sub_prop}, $selector, $wrapped_params, $processors);
          }
        }
        else if ($list_type = entity_property_list_extract_type($property['type'])) {
          // This is a list so we apply over and over again for all the items.
          $sub_select_items = $value['sub_select'];
          if (!is_numeric(key($value['sub_select']))) {
            $sub_select_items = array($value['sub_select']);
          }

          foreach ($sub_select_items as $sub_delta => $sub_info) {
            // Prepare the wrapper.
            // Special case for field collections.
            if ($list_type == 'field_collection_item') {
              $item = &$entity->{$prop}[LANGUAGE_NONE][$sub_delta];
              $field_collection_item = field_collection_field_get_entity($item, $prop);
              $prop_wrapper = entity_metadata_wrapper('field_collection_item', $field_collection_item);
            }
            else {
              $prop_wrapper = $wrapper->{$prop}[$sub_delta];
            }

            // Get sub property info.
            $sub_properties = $prop_wrapper->getPropertyInfo();
            foreach ($sub_info as $sub_prop => $options) {
              if (empty($sub_properties[$sub_prop])) {
                continue;
              }

              if (!empty($options['parameter']['select'])) {
                // Prepare the processors.
                $processors = NULL;
                if (!empty($options['parameter']['select_template_options'])) {
                  $processors = $options['parameter']['select_template_options'];
                  RulesDataProcessor::prepareSetting(
                    $processors,
                    $sub_properties[$sub_prop],
                    $parameter_info
                  );
                }
                $this->applyDataSelector($prop_wrapper->{$sub_prop}, $options['parameter']['select'], $wrapped_params, $processors);
              }
              else if (!empty($options['direct_input'])) {
                if ($prop_wrapper->{$sub_prop} instanceof EntityDrupalWrapper || $prop_wrapper->{$sub_prop} instanceof EntityValueWrapper) {
                  if (!empty($options['direct_input']['element'])) {
                    $prop_wrapper->{$sub_prop} = $options['direct_input']['element'];
                  }
                }
                else {
                  $prop_wrapper->{$sub_prop} = $options['direct_input'];
                }
              }
            }
          }
        }
        else {
          // Single item with sub properties.
          $prop_wrapper = $wrapper->{$prop};
          $sub_properties = $prop_wrapper->getPropertyInfo();

          foreach ($value['sub_select'] as $sub_prop => $options) {
            // @todo: Can we move this to a method?
            if (empty($sub_properties[$sub_prop])) {
              continue;
            }

            if (!empty($options['parameter']['select'])) {
              // Prepare the processors.
              $processors = NULL;
              if (!empty($options['parameter']['select_template_options'])) {
                $processors = $options['parameter']['select_template_options'];
                RulesDataProcessor::prepareSetting(
                  $processors,
                  $sub_properties[$sub_prop],
                  $parameter_info
                );
              }
              $this->applyDataSelector($prop_wrapper->{$sub_prop}, $options['parameter']['select'], $wrapped_params, $processors);
            }
            else if (!empty($options['direct_input'])) {
              if ($prop_wrapper->{$sub_prop} instanceof EntityDrupalWrapper || $prop_wrapper->{$sub_prop} instanceof EntityValueWrapper) {
                if (!empty($options['direct_input']['element'])) {
                  $prop_wrapper->{$sub_prop} = $options['direct_input']['element'];
                }
              }
              else {
                $prop_wrapper->{$sub_prop} = $options['direct_input'];
              }
            }
          }
        }
      }
      else if ($method == 'select') {
        // Apply the selector.
        $processors = NULL;
        if (!empty($value['select_template_options'])) {
          $processors = $value['select_template_options'];
          RulesDataProcessor::prepareSetting(
            $processors,
            $property,
            $parameter_info
          );
        }
        $selector = $value['select'];
        $this->applyDataSelector($wrapper->{$prop}, $selector, $wrapped_params, $processors);
      }
      else if ($method == 'direct' && empty($property['field'])) {
        $entity->{$prop} = $value['value'];
      }
      else if ($method == 'direct') {
        $entity->{$prop}[LANGUAGE_NONE] = !empty($value[LANGUAGE_NONE]) ? $value[LANGUAGE_NONE] : array();
      }

      // Get rid of empty field collection items.
      if (in_array($property['type'], array('field_collection_item', 'list<field_collection_item>'))) {
        foreach ($entity->{$prop}[LANGUAGE_NONE] as $fc_delta => $fc_item) {
          $fc_entity = field_collection_field_get_entity($fc_item);
          if (field_collection_item_is_empty($fc_entity)) {
            unset($entity->{$prop}[LANGUAGE_NONE][$fc_delta]);
          }
        }
        $entity->{$prop}[LANGUAGE_NONE] = array_values($entity->{$prop}[LANGUAGE_NONE]);
      }
    }

    // Perform ctools style substitutions.
    $parameters['self'] = $entity;
    foreach ($entity as $prop => &$value) {
      if (is_string($value)) {
        $value = $this->replaceCtoolsSubstitutions($value, $parameters);
      }
      else if (($field_info = field_info_field($prop)) && is_array($value)) {
        foreach ($value as $langcode => &$langitem) {
          if (!is_array($langitem) && !($langitem instanceof Iterator)) {
            continue;
          }

          foreach ($langitem as $delta => &$item) {
            if (!is_numeric($delta)) {
              continue;
            }

            // Special handling for field collections.
            // We turn the 'entity' array back into a field collection item
            // object.
            if ($field_info['type'] == 'field_collection') {
              if (is_array($item['entity'])) {
                $item['entity'] = entity_create('field_collection_item', $item['entity']);
              }

              // Do tokens on string fields on the field collection item.
              foreach ($item['entity'] as &$item_prop) {
                if (!is_array($item_prop)) {
                  continue;
                }

                foreach ($item_prop as $item_langcode => &$item_langitem) {
                  if (!is_array($item_langitem)) {
                    continue;
                  }

                  foreach ($item_langitem as $item_delta => &$item_item) {
                    if (!is_numeric($item_delta)) {
                      continue;
                    }

                    foreach ($item_item as $item_key => &$item_val) {
                      if (is_string($item_val)) {
                        $item_val = $this->replaceCtoolsSubstitutions($item_val, $parameters);
                      }
                    }
                  }
                }
              }
            }

            foreach ($item as $key => &$val) {
              if (is_string($val)) {
                $val = $this->replaceCtoolsSubstitutions($val, $parameters);
              }
            }
          }
        }
      }
    }

    // Finaly tell the entity which template made it.
    $entity->entity_template = $this->template->name;

    module_invoke_all('entity_template_create_entity', $this, $entity, $this->template->getTargetEntityType());
    rules_invoke_event('entity_template_create_entity', $this, entity_metadata_wrapper($this->template->getTargetEntityType(), $entity));

    return $entity;
  }

  /**
   * Build a list of possible ctools substitutions.
   *
   * @param array $keywords
   *   Optionally provide additional keywords to show.
   *
   * @return array
   *   A render array of substitutions.
   */
  public function getCtoolsSubstitutionsList($keywords = array()) {
    $content = array(
      '#theme' => 'table',
      '#header' => array(t('Keyword'), t('Value')),
      '#rows' => array(),
    );

    foreach ($this->getCtoolsContexts() as $context) {
      foreach (ctools_context_get_converters('@:' . check_plain($context->keyword) . ':', $context) as $keyword => $title) {
        $content['#rows'][] = array(
          check_plain($keyword),
          t('@identifier: @title', array('@title' => $title, '@identifier' => $context->identifier)),
        );
      }
    }

    if (count($content['#rows'])) {
      return $content;
    }
    else {
      return array();
    }
  }

  /**
   * Get a nested property.
   */
  function getNestedWrapper($wrapped_params, $selector) {
    $parts = explode(':', $selector);
    $namespace = str_replace('-', '_', array_shift($parts));
    $selected = $wrapped_params[$namespace];

    if (empty($selected)) {
      return FALSE;
    }

    try {
      while (($sub = array_shift($parts)) !== NULL) {
        try {
          $sub = str_replace('-', '_', $sub);
          $selected = $selected->get($sub);
        }
        catch (EntityMetadataWrapperException $e) {
          return FALSE;
        }
      }
    }
    catch (Exception $e) {
      return FALSE;
    }

    return $selected;
  }

  /**
   * Apply a data selector to a property.
   *
   * @param EntityMetadataWrapper $wrapper
   *   The wrapper of property to set.
   * @param $selector
   *   The selector to find the data value.
   * @param $params
   *   The wrapper parameters.
   * @param RulesDataProcessor $processors
   *   Rules data processor to tweak values/
   */
  function applyDataSelector($wrapper, $selector, $wrapped_params, $processor = NULL) {
    // Special behaviour for lists.
    // For lists we allow the use of a comma separated list of properties
    // these properties can be lists in their own right or individual
    // instances of whatever the list contains.
    if ($wrapper instanceof EntityListWrapper) {
      $selectors = explode(',', $selector);
      foreach ($selectors as $sel) {
        if ($selected = $this->getNestedWrapper($wrapped_params, $sel)) {
          if ($selected instanceof EntityListWrapper) {
            // Selected is a list, so loop over what we have and add them to
            // the property.
            foreach ($selected as $item) {
              try {
                if ($item->value()) {
                  $wrapper[] = $item->value();
                }
              }
              catch (EntityMetadataWrapperException $e) {}
            }
          }
          else {
            // Get the value from the selected wrapper. We wrap this in a try
            // block to make sure that the system doesn't completely fail when
            // one of the parent values is missing.
            try {
              if (is_callable(array($selected, 'value')) && $selected->value()) {
                $wrapper[] = $selected->value();
              }
            }
            catch (EntityMetadataWrapperException $e) {}
          }
        }
      }
    }
    else {
      $selected = $this->getNestedWrapper($wrapped_params, $selector);
      try {
        if (is_object($selected) && $selected->value()) {
          $value = $selected->value();
          if (!empty($processor)) {
            $state = new RulesState();
            foreach ($wrapped_params as $name => $param_wrapper) {
              if ($name == 'site') {
                continue;
              }
              $state->addVariable($name, $param_wrapper->value(), $param_wrapper->info());
            }

            $value = $processor->process($value, $wrapper->info(), $state, new RulesAction('entity_template_create'));
          }
          $wrapper->set($value);
        }
      }
      catch (EntityMetadataWrapperException $e) {}
    }
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, array &$form_state): array {
    $form['template'] = array(
      '#type' => 'vertical_tabs',
      '#title' => t('Template'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#prefix' => '<div id="template-form">',
      '#suffix' => '</div>',
      '#parents' => array('template'),
      '#theme_wrappers' => array('vertical_tabs', 'fieldset'),
      '#tree' => TRUE,
    );

    if (!empty($this->template->getTargetEntityType()) && !empty($this->template->getTargetBundle())) {
      $op = 'edit';
      if (empty($this->template->getTemplate())) {
        $op = 'create';
      }
      $form['template'] += entity_template_get_entity_form(
        $form['template'],
        $form_state,
        $this->template->getTargetEntityType(),
        $this->buildTemplate(),
        'edit'
      );
    }

    $form['substitutions'] = array(
      '#title' => t('Substitutions'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['substitutions']['table'] = $this->getCtoolsSubstitutionsList();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationFormValidate(array $form, array &$form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function configurationFormSubmit(array $form, array &$form_state) {
    $entity = $this->buildTemplate();
    $entity_info = entity_get_info($this->template->getTargetEntityType());
    $wrapper = entity_metadata_wrapper($this->template->getTargetEntityType(), $entity);
    $form_fields = [
      '#parents' => ['template'],
    ];
    foreach ($wrapper->getPropertyInfo() as $key => $property_info) {
      if (!empty($property_info['field'])) {
        $form_fields[$key] = $form['template'][$key.'--tab']['group']['direct_input']['element'];
      }
    }
    field_attach_submit($this->template->getTargetEntityType(), $entity, $form_fields, $form_state);
    unset($entity->__template);

    // Store other selections.
    foreach ($wrapper->getPropertyInfo() as $key => $property_info) {
      if (empty($property_info['setter callback']) || $key === $entity_info['entity keys']['bundle']) {
        if (
          $key != $entity_info['entity keys']['bundle'] && property_exists($entity, $key) &&
          (new ReflectionProperty($entity, $key))->isPublic()
        ) {
          unset($entity->{$key});
        }

        continue;
      }

      $group = $form_state['values']['template'][$key.'--tab']['group'];

      $value = [
        'method' => $group['method'],
      ];
      switch ($group['method']) {
        case 'direct':
          if (!empty($property_info['field'])) {
            $value[LANGUAGE_NONE] = $entity->{$key}[LANGUAGE_NONE];
          }
          else {
            $value['value'] = $group['direct_input']['element'];
          }
          break;
        case 'select':
          $value['select'] = !empty($group['parameter']['select']) ? preg_replace('/\s+/', '', $group['parameter']['select']) : FALSE;
          $value['select_template_options'] = !empty($group['parameter']['select_template_options']) ? $group['parameter']['select_template_options'] : [];
          break;
        case 'sub_select':
          $value['sub_select'] = (!empty($group['parameter_sub']) && $group['method'] == 'sub_select') ?
            (!empty($group['parameter_sub']['items']) ? $group['parameter_sub']['items'] : $group['parameter_sub']) :
            [];
          unset($value['sub_select']['add_more']);

          if ($group['parameter_sub']['items']) {
            foreach (array_keys($value['sub_select']) as $delta) {
              foreach (array_keys($value['sub_select'][$delta]) as $property) {
                $value['sub_select'][$delta][$property] = array_filter($value['sub_select'][$delta][$property], fn($k) => in_array($k, array('direct_input', 'parameter')), ARRAY_FILTER_USE_KEY);
              }
            }
          }
          else {
            foreach (array_keys($value['sub_select']) as $property) {
              $value['sub_select'][$property] = array_filter($value['sub_select'][$property], fn($k) => in_array($k, array('direct_input', 'parameter')), ARRAY_FILTER_USE_KEY);
            }
          }

          break;
      }

      $entity->{$key} = $value;
    }

    $this->template->setTemplate(entity_get_controller($this->template->getTargetEntityType())->export($entity));
  }

}
