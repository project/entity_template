<?php

/**
 * Builds the entity template.
 */
abstract class EntityTemplateBuilderBase implements EntityTemplateBuilderInterface {

  /**
   * The template.
   *
   * @var \EntityTemplateInterface|null
   */
  protected $template = NULL;

  /**
   * Construct a new builder.
   *
   * @param \EntityTemplate $template
   */
  public function __construct(EntityTemplateInterface $template) {
    $this->template = $template;
  }

  /**
   * Prepare a blank template.
   */
  protected function prepareBlankTemplate() {
    $info = entity_get_info($this->template->getTargetEntityType());
    $values = array();
    if (!empty($info['entity keys']['bundle'])) {
      $values[$info['entity keys']['bundle']] = $this->template->getTargetBundle();
    }
    $entity = entity_create($this->template->getTargetEntityType(), $values);
    module_invoke_all('entity_template_prepare_blank_template', $this->template, $entity, $this->template->getTargetEntityType());
    return $entity;
  }

  /**
   * Get an array of ctools context for the flexiform.
   *
   * @param array $entities
   *   An array of entities keyed by namespace. If NULL, placeholder contexts
   *   will be used.
   *
   * @return ctools_context[]
   *   An array of ctools contexts.
   */
  protected function getCtoolsContexts($parameters = array()) {
    global $user;

    ctools_include('context');
    $contexts = array(
      'site' => ctools_context_create('token'),
      'current-user' => ctools_context_create('entity:user', $user),
      'self' => !empty($parameters['self']) ?
        ctools_context_create('entity:' . $this->template->getTargetEntityType(), $parameters['self']) :
        ctools_context_create_empty('entity:' . $this->template->getTargetEntityType())
    );
    $contexts['site']->keyword = 'global';
    $contexts['current-user']->keyword = 'current-user';
    $contexts['current-user']->identifier = t('Logged-in user');
    $contexts['self']->keyword = 'self';

    $available_context_plugins = ctools_get_contexts();
    if (is_array($this->template->getParameters())) {
      foreach ($this->template->getParameters() as $namespace => $info) {
        if (entity_get_info($info['type'])) {
          $type = 'entity:' . $info['type'];
        }
        else if ($info['type'] == 'text') {
          $type = 'string';
        }
        else if (!empty($available_context_plugins[$info['type']])) {
          $type = $info['type'];
        }
        else {
          continue;
        }

        if (!empty($parameters[$namespace])) {
          $contexts[$namespace] = ctools_context_create($type, $parameters[$namespace]);
        }
        else {
          $contexts[$namespace] = ctools_context_create_empty($type);
        }
        $contexts[$namespace]->keyword = $namespace;
        $contexts[$namespace]->identifier = $info['label'];
      }
    }

    return $contexts;
  }

  /**
   * Replace ctools substitutions with their values.
   *
   * To allow conventional ctools tokens to work in the actual entity that gets
   * created we use a slightly different syntax. Tokens start with @: instead
   * of %.
   *
   * @param string $string
   *   The string we want to replace in.
   * @param array $parameters
   *   An array of entities keyed by namespace. If NULL, placeholder contexts
   *   will be used.
   * @param array $keywords
   *   Optionally provide additional keywords to replace.
   *
   * @return string
   *   $string with it's substitutions replaced.
   */
  protected function replaceCtoolsSubstitutions($string, $parameters, $keywords = array()) : string  {
    $contexts = $this->getCtoolsContexts($parameters);

    // Convert normal ctools tokens into ignorable ctools tokens.
    $string = str_replace('%', '@|', $string);
    $string = str_replace('@:', '%', $string);
    $string = ctools_context_keyword_substitute($string, $keywords, $contexts, array('sanitize' => FALSE));
    $string = str_replace('@|', '%', $string);
    return $string;
  }

}

/**
 * Interface for template builders.
 */
interface EntityTemplateBuilderInterface {

  /**
   * Build the template into something like an entity that can be edited.
   *
   * @return \Entity|\stdClass|object
   *   The entity template object ready to be manipulated.
   *
   * @todo This method should become protected.
   */
  public function buildTemplate() : object;

  /**
   * Execute the template.
   *
   * @param array $parameters
   *   The parameters to use for the template.
   * @param ?\EntityDrupalWrapper $target
   *   The target entity to apply the template to.   *
   *
   * @return \EntityInterface|\stdClass|object
   *   The entity contructed from the template.
   */
  public function executeTemplate(array $parameters, EntityDrupalWrapper $target = NULL) : object;

  /**
   * Build the configuration form for this template.
   *
   * @param array $form
   *   The form.
   * @param array $form_state
   *   The form state.
   *
   * @return array
   *   The constructed configuration form.
   */
  public function configurationForm(array $form, array &$form_state) : array;

  /**
   * Validate the configuration form.
   *
   * @param array $form
   *   The form.
   * @param array $form_state
   *   The form state.
   *
   * @return void
   */
  public function configurationFormValidate(array $form, array &$form_state);

  /**
   * Submit the configuration form.
   *
   * @param array $form
   *   The form.
   * @param array $form_state
   *   The form state.
   *
   * @return void
   */
  public function configurationFormSubmit(array $form, array &$form_state);

}
