<?php
/**
 * @file
 *   Rules Integration with Entity Templates.
 */

/**
 * Implements hook_rules_event_info().
 */
function entity_template_rules_event_info() {
  $items = array();
  $info = entity_get_info('entity_template');
  $info += array('rules controller class' => 'EntityDefaultRulesController');
  if ($info['rules controller class']) {
    $controller = new $info['rules controller class']('entity_template');
    $items += $controller->eventInfo();
  }

  $items['entity_template_create_entity'] = array(
    'label' => t('When an entity is created from a template'),
    'group' => t('Entity'),
    'variables' => array(
      'template' => array(
        'label' => t('Template'),
        'type' => 'entity_template',
      ),
      'entity' => array(
        'label' => t('Created Entity'),
        'type' => 'entity',
        'skip save' => TRUE,
      ),
    ),
  );

  return $items;
}

/**
 * Implements hook_rules_action_info().
 */
function entity_template_rules_action_info() {
  $actions = array();
  $entity_info = entity_get_info();

  $actions['entity_template_create'] = array(
    'label' => t('Create a new Entity from a Template'),
    'named parameter' => TRUE,
    'group' => t('Entity Templates'),
    'parameter' => array(
      'entity_template' => array(
        'type' => 'entity_template',
        'label' => t('Template'),
        'options list' => 'entity_template_rules_template_options',
        'description' => t('Specifies the type of the entity that should be created.'),
        'restriction' => 'input',
      ),
      // Further needed parameter depends on the type.
    ),
    'provides' => array(
      'entity_created' => array(
        'type' => 'unknown',
        'label' => t('Created entity'),
        'save' => TRUE,
      ),
    ),
    'base' => 'entity_template_rules_action_template_create',
    'callbacks' => array(
      'form_alter' => 'entity_template_rules_action_template_create_form_alter',
    ),
  );

  return $actions;
}

/**
 * Entity Template options.
 */
function entity_template_rules_template_options() {
  $templates = entity_load('entity_template');
  $entity_info = entity_get_info();
  $options = array();

  foreach ($templates as $id => $template) {
    $entity_type = $entity_info[$template->entity_type]['label'];
    $options[$entity_type][$template->name] = $template->label;
  }

  return $options;
}

/**
 * Rules create a new entity from a template.
 */
function entity_template_rules_action_template_create($args, $element) {
  $params = array();
  foreach ($element->pluginParameterInfo() as $name => $info) {
    if ($name != 'entity_template') {
      // Remove the parameter name prefix 'param_'.
      $params[substr($name, 6)] = $args[$name];
    }
  }
  try {
    $data = entity_create_from_template($args['entity_template'], $params);
    return array('entity_created' => $data);
  }
  catch (EntityMetadataWrapperException $e) {
    throw new RulesEvaluationException('Unable to create entity from template @template": ' . $e->getMessage(), array('@template' => $args['entity_template']->label), $element);
  }
}

/**
 * Info alter hook for creating a entity from a template.
 */
function entity_template_rules_action_template_create_info_alter(&$element_info, RulesAbstractPlugin $element) {
  if (!empty($element->settings['entity_template'])) {
    $template = entity_load_single('entity_template', $element->settings['entity_template']);
    $entity_info = entity_get_info($template->entity_type);
    if (!empty($template->parameters)) {
      foreach ($template->parameters as $key => $settings) {
        $element_info['parameter']['param_'.$key] = array(
          'type' => $settings['type'],
          'label' => $settings['label'],
          'optional' => !empty($settings['optional']),
        );

        if (!empty($settings['bundle'])) {
          $element_info['parameter']['param_'.$key]['bundle'] = $settings['bundle'];
        }
      }
    }
    $element_info['provides']['entity_created']['type'] = $template->entity_type;
    if (!empty($entity_info['entity keys']['bundle'])) {
      $element_info['provides']['entity_created']['bundle'] = $template->bundle;
    }
  }
}

function entity_template_rules_action_template_create_form_alter(&$form, &$form_state, $options, RulesAbstractPlugin $element) {
  $first_step = empty($element->settings['entity_template']);
  $form['reload'] = array(
    '#weight' => 5,
    '#type' => 'submit',
    '#name' => 'reload',
    '#value' => $first_step ? t('Continue') : t('Reload form'),
    '#limit_validation_errors' => array(array('parameter', 'entity_template')),
    '#submit' => array('rules_action_type_form_submit_rebuild'),
    '#ajax' => rules_ui_form_default_ajax(),
  );
  // Use ajax and trigger as the reload button.
  $form['parameter']['entity_template']['settings']['entity_template']['#ajax'] = $form['reload']['#ajax'] + array(
    'event' => 'change',
    'trigger_as' => array('name' => 'reload'),
  );

  if ($first_step) {
    // In the first step show only the type select.
    foreach (element_children($form['parameter']) as $key) {
      if ($key != 'entity_template') {
        unset($form['parameter'][$key]);
      }
    }
    unset($form['submit']);
    unset($form['provides']);
    // Disable #ajax for the first step as it has troubles with lazy-loaded JS.
    // @todo: Re-enable once JS lazy-loading is fixed in core.
    unset($form['parameter']['entity_template']['settings']['entity_template']['#ajax']);
    unset($form['reload']['#ajax']);
  }
  else {
    // Hide the reload button in case js is enabled and it's not the first step.
    $form['reload']['#attributes'] = array('class' => array('rules-hide-js'));
  }
}
