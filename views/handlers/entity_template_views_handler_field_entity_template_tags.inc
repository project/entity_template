<?php

/**
 * @file
 * Contians entity_template_views_handler_field_entity_template_tags
 */

class entity_template_views_handler_field_entity_template_tags extends views_handler_field {
  function construct() {
    $this->additional_fields['id'] = 'id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $id = $this->get_value($values, 'id');
    $template = entity_load_single('entity_template', $id);
    return implode(', ', $template->tags);
  }
}
