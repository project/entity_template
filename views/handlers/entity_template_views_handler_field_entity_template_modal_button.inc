<?php

/**
 * @file
 * Contians entity_template_views_handler_field_entity_template_modal_button
 */

class entity_template_views_handler_field_entity_template_modal_button extends views_handler_field {
  function construct() {
    $this->additional_fields['name'] = 'name';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['get_query'] = array('default' => '');
    $options['button_class'] = array('default' => 'button');
    $options['button_text'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['get_query'] = array(
      '#type' => 'textarea',
      '#title' => t('Query Options'),
      '#description' => t('Enter any additional parameters you wish to send into this box. One line per perameter of the form key|value'),
      '#default_value'=> $this->options['get_query'],
    );

    $form['button_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Button Text'),
      '#description' => t('The text to display on the button.'),
      '#default_value' => $this->options['button_text'],
    );

    $form['button_class'] = array(
      '#type' => 'textfield',
      '#title' => t('Button Class'),
      '#description' => t('CSS classes to apply to the button.'),
      '#default_value' => $this->options['button_class'],
    );

    parent::options_form($form, $form_state);
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $name = $this->get_value($values, 'name');

    $tokens = $this->get_render_tokens(array());
    $query = strtr($this->options['get_query'], $tokens);
    $title = strtr($this->options['button_text'], $tokens);
    $class = strtr($this->options['button_class'], $tokens);

    if (empty($title)) {
      $title = t('Create');
    }

    ctools_include('ajax');
    ctools_include('modal');

    $path = "template/add/{$name}/ajax";

    // Handle the http get query.
    $params = array();
    $list = explode("\n", $query);
    $list = array_map('trim', $list);
    $list = array_filter($list, 'strlen');
    foreach ($list as $text) {
      list($key, $value) = explode('|', $text, 2);
      $params[$key] = $value;
    }

    $content = '';

    if (drupal_valid_path($path)) {
      $content = ctools_modal_text_button($title, url($path, array('absolute' => TRUE, 'query' => $params)), $title, $class);
    }

    return $content;
  }
}
