<?php
/**
 * @file
 *   Views integration for entity template.
 */

/**
 * Implements hook_views_data_alter().
 */
function entity_template_views_data_alter(&$data) {
  $table = &$data['entity_template_revision'];
  $table['id']['relationship'] = array(
    'label' => t('Template Entity'),
    'handler' => 'views_handler_relationship',
    'base' => 'entity_template',
    'base field' => 'id',
    'relationship field' => 'id',
  );

  $table['log'] = array(
    'title' => t('Log Message'),
    'help' => t('The log message to go with the revision.'),
    'field' => array(
      'real field' => 'log',
      'handler' => 'views_handler_field',
    ),
  );
  $table['timestamp'] = array(
    'title' => t('Timestamp'),
    'help' => t('The revision timestamp.'),
    'field' => array(
      'real field' => 'timestamp',
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
      'is date' => TRUE,
    ),
    'sort' => array(
      'real field' => 'timestamp',
      'handler' => 'views_handler_sort_date',
      'is date' => TRUE,
    ),
  );
  $table['uid'] = array(
    'title' => t('Creator'),
    'help' => t('The User who Created the Revision'),
    'relationship' => array(
      'title' => t('Creator'),
      'help' => t('Relate revision to the user who created it.'),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'field' => 'uid',
      'label' => t('creator'),
    ),
    'field' => array(
      'handler' => 'views_handler_field_user',
    ),
  );

  $table = &$data['entity_template'];
  $table['modal_button'] = array(
    'title' => t('Modal Button'),
    'help' => t('Display a modal button to create an entity with this template.'),
    'field' => array(
      'handler' => 'entity_template_views_handler_field_entity_template_modal_button',
    ),
  );
  $table['tags'] = array(
    'title' => t('Tags'),
    'help' => t('Comma separated list of tags.'),
    'field' => array(
      'handler' => 'entity_template_views_handler_field_entity_template_tags',
    ),
  );

  $data['entity_template_tags'] = array(
    'table' => array(
      'group' => t('Entity Template'),
      'join' => array(
        'entity_template' => array(
          'left_field' => 'vid',
          'field' => 'vid',
        ),
        'entity_template_revision' => array(
          'left_field' => 'vid',
          'field' => 'vid',
        ),
      ),
    ),
  );
  $data['entity_template_tags']['tag'] = array(
    'title' => t('Tag'),
    'help' => t('The tags associated with this entity template.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_many_to_one',
      'options callback' => 'entity_template_tags_options',
    ),
  );
}
